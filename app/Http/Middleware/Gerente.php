<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class Gerente
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::user()->tipo_user == 2){
        return redirect()->route('ventas_registradas.index');
        }else{
        return redirect()->route('ventas_registradas.index');
        }


         return $next($request);
    }
}