<?php

namespace App\Http\Middleware;

use Closure;
use Redirect;
use Session;
class Admin
{


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if ($role === 1 || $role === 2) {
            return Redirect::route('welcome');
        }
        else {
            return Redirect::route('login');
        }
        return $next($request);
    }
}
