<?php

namespace App\Http\Controllers\Auth;
use Hash;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/home';
    



    public function authenticated($request , $user){
        
        if($user->tipo_user =='1'){
            return redirect()->route('home');
        }elseif($user->tipo_user =='2'){
            return redirect()->route('ventas_registradas.index');
        }elseif($user->tipo_user =='3'){
            return redirect()->route('ventas.index');
            
        }
    }

    //public function logout()
    //{
    //    Auth::logout();
    //    return redirect()->to('/');
    //}

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except([
            'logout',
        ]);
    }

}
