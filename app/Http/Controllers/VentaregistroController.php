<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Ventas;
use App\Articulos;
use App\Ticket_Venta;



class VentaregistroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       $datos['venta_registrada_table'] = Ventas::where([['activo','1']])->orderBy('id','DESC')->get();

        return view('ventas_registradas.index')->with($datos);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        

        return view('ventas_registradas.create');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
     public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $datos['articulos'] = Articulos::where([['activo','1']])->get();
        $datos['ventas'] = Ventas::find($id);
        $datos['articulos'] = Ticket_Venta::where([
            ['activo',1],
            ['id_venta',$id]
        ])->get();
        return view('ventas_registradas.create')->with($datos);    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        try {
            
            $venta = Ventas::find($id);
            $venta->fill($request->all());
            $venta->save();
        
           return response()->json(['success'=>'Se ha Editado con Exito']);

        } catch (\Exception $e) {
          dd($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $venta = Ventas::find($id);
        $venta->activo = 0;
        $venta->save();



        return redirect()->route('ventas_registradas.index');
    }






}
