<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;


use App\TipoUsuario; 


class TipoUsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       $datos['tipo_usuario_table'] = TipoUsuario::where([['activo','1']])->orderBy('id','DESC')->get();

        return view('tipo_usuarios.index')->with($datos);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        

        return view('tipo_usuarios.create');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            try {

                $tipo_usuarios = new TipoUsuario();
                $tipo_usuarios->nombre = $request->nombre;
                $tipo_usuarios->save();



                return response()->json(['success'=>'Se ha Agegado con Exito']);
            } catch (\Exception $e) {
              dd($e->getMessage());
            }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
     public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $datos['tipo_usuarios'] = TipoUsuario::find($id);
        return view('tipo_usuarios.create')->with($datos);    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        try {
            
            $tipo_usuarios = TipoUsuario::find($id);
            $tipo_usuarios->fill($request->all());
            $tipo_usuarios->save();
        
           return response()->json(['success'=>'Se ha Editado con Exito']);

        } catch (\Exception $e) {
          dd($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tipo_usuarios = TipoUsuario::find($id);
        $tipo_usuarios->activo = 0;
        $tipo_usuarios->save();



        return redirect()->route('tipo_usuarios.index');
    }






}
