<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\User;
use App\Vendedor;
use App\TipoUsuario;
use App\Menu;
use App\Permisos;
use Auth;


class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       $datos['usuarios_table'] = User::where([['activo','1'],['id_personal','!=','0']])->orderBy('id','DESC')->get();

        return view('usuarios.index')->with($datos);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $datos['tipo_usuario'] = TipoUsuario::where('activo','1')->get();
        $datos['menu'] = Menu::where('activo','1')->get();
        
        return view('usuarios.create')->with($datos);        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            try {

                if ($request->hasFile('avatar')) {
                    $file = $request->file('avatar');
                   
                    $name = time().$file->getClientOriginalName();

                    $file->move(public_path().'/images/',$name);


                    $tienda = new User();
                    $tienda->name = $request->name;
                    $tienda->email = $request->email;
                    $tienda->password = bcrypt($request->password);
                    $tienda->tipo_user = $request->tipo_user;
                    $tienda->imagen_usuario = $name;
                    $tienda->save();
                  
                }else{
                    
                    $tienda = new User();
                    $tienda->name = $request->name;
                    $tienda->email = $request->email;
                    $tienda->password = bcrypt($request->password);
                    $tienda->tipo_user = $request->tipo_user;
                    $tienda->save();
                
                
                } 

                


                return redirect()->route('usuarios.index');
                //return response()->json(['success'=>'Se ha Agegado con Exito']);
            } catch (\Exception $e) {
              dd($e->getMessage());
            }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
     public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $datos['tipo_usuario'] = TipoUsuario::where('activo','1')->get();
        $datos['usuarios'] = User::find($id);
        $datos['menu'] = Menu::where('activo','1')->get();
        $datos['cuantas'] = Menu::where([['activo','1']])->count();

        $usuario = Auth::user()->id_peronal;


        $datos['permisos'] = Permisos::
        join('menu', 'permisos.id_permisos', '=', 'menu.id')->
        select(
                'permisos.id',
             'permisos.id_permisos',
              'menu.nombre'
          )->where([
          ['permisos.activo', '=', '1'],
          ['permisos.id_usuario', '=', $datos['usuarios']->id_personal],

        ])->get();

         
        return view('usuarios.create')->with($datos);    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
            

        try {
            
            if ($request->hasFile('avatar')) {
                    $file = $request->file('avatar');
                   
                    $name = time().$file->getClientOriginalName();

                    $file->move(public_path().'/images/',$name);

                   

                    $tienda = User::find($id);
                    $tienda->name = $request->name;
                    $tienda->email = $request->email;
                    $tienda->id_personal = $request->id_personal;
                    $tienda->password = bcrypt($request->password);
                    $tienda->password_name = $request->password;
                    $tienda->tipo_user = $request->tipo_user;
                    $tienda->imagen_usuario = $name;
                    $tienda->save();
                  
                }else{

                    

                    $tienda = User::find($id);
                    $tienda->name = $request->name;
                    $tienda->email = $request->email;
                    $tienda->id_personal = $request->id_personal;
                    $tienda->password = bcrypt($request->password);
                    $tienda->password_name = $request->password;
                    $tienda->tipo_user = $request->tipo_user;
                    $tienda->save();
                
                
                }            
            


            Vendedor::where([['activo','1'],['id',$tienda->id_personal]])->update([
              "password" => $request->password
            ]);
            return redirect()->route('usuarios.index');
           //return response()->json(['success'=>'Se ha Editado con Exito']);

        } catch (\Exception $e) {
          dd($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tienda = User::find($id);
        $tienda->activo = 0;
        $tienda->save();


        Vendedor::where([['activo','1'],['id',$id]])->update([
          "activo" => 0
        ]);

        return redirect()->route('usuarios.index');
    }


    public function permisos(Request $request)
    {


        
        try {

        foreach ($request->permisos as $value) {
            $permisos = new Permisos();
            $permisos->id_usuario = $request->id_personal;
            $permisos->id_permisos = $value['permiso_id'];
            $permisos->tipo_user = $request->tipo_user;
            $permisos->id = $request->id;
            $permisos->save();
        }
        

        return response()->json(['success'=>'Se han Agegado los Permisos']);

        } catch (\Exception $e) {
          dd($e->getMessage());
        }
    }


    public function borrar_permisos(Request $request){
       
        try {
            $permisos = Permisos::find($request->id);
            $permisos->activo = 0;
            $permisos->save();


        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }






}
