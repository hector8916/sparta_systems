<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;


use App\Configuracion; 


class ConfiguracionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       $datos['configuraciones_table'] = Configuracion::where([['activo','1']])->orderBy('id','DESC')->get();

        return view('configuraciones.index')->with($datos);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        

        return view('configuraciones.create');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            try {


                if ($request->hasFile('avatar')) {
                    $file = $request->file('avatar');
                   
                    $name = time().$file->getClientOriginalName();

                    $file->move(public_path().'/images/',$name);


                    $configuraciones = new Configuracion();
                    $configuraciones->nombre_empresa = $request->nombre_empresa;
                    $configuraciones->nombre_corto = $request->nombre_corto;
                    $configuraciones->direccion = $request->direccion;
                    $configuraciones->cp = $request->cp;
                    $configuraciones->correo_electronico = $request->correo_electronico;
                    $configuraciones->telefono = $request->telefono;
                    $configuraciones->logo_empresa = $name;
                    $configuraciones->cambiar_skin = $request->cambiar_skin;
                    $configuraciones->save();
                  
                }else{


                    $configuraciones = new Configuracion();
                    $configuraciones->nombre_empresa = $request->nombre_empresa;
                    $configuraciones->nombre_corto = $request->nombre_corto;
                    $configuraciones->direccion = $request->direccion;
                    $configuraciones->cp = $request->cp;
                    $configuraciones->correo_electronico = $request->correo_electronico;
                    $configuraciones->telefono = $request->telefono;
                    $configuraciones->cambiar_skin = $request->cambiar_skin;
                    $configuraciones->save();
                
                
                }



                

                return redirect()->route('configuraciones.index');
                //return response()->json(['success'=>'Se ha Agegado con Exito']);
            } catch (\Exception $e) {
              dd($e->getMessage());
            }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
     public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $datos['configuraciones'] = Configuracion::find($id);
        return view('configuraciones.create')->with($datos);    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        try {


            if ($request->hasFile('avatar')) {
                    $file = $request->file('avatar');
                   
                    $name = time().$file->getClientOriginalName();

                    $file->move(public_path().'/images/',$name);


                    $configuraciones = Configuracion::find($id);
                    $configuraciones->nombre_empresa = $request->nombre_empresa;
                    $configuraciones->nombre_corto = $request->nombre_corto;
                    $configuraciones->direccion = $request->direccion;
                    $configuraciones->cp = $request->cp;
                    $configuraciones->correo_electronico = $request->correo_electronico;
                    $configuraciones->telefono = $request->telefono;
                    $configuraciones->logo_empresa = $name;
                    $configuraciones->cambiar_skin = $request->cambiar_skin;
                    $configuraciones->save();
                  
                }else{


                    $configuraciones = Configuracion::find($id);
                    $configuraciones->nombre_empresa = $request->nombre_empresa;
                    $configuraciones->nombre_corto = $request->nombre_corto;
                    $configuraciones->direccion = $request->direccion;
                    $configuraciones->cp = $request->cp;
                    $configuraciones->correo_electronico = $request->correo_electronico;
                    $configuraciones->telefono = $request->telefono;
                    $configuraciones->cambiar_skin = $request->cambiar_skin;
                    $configuraciones->save();
                
                
                }
            

           return redirect()->route('configuraciones.index');
           
           //return response()->json(['success'=>'Se ha Editado con Exito']);

        } catch (\Exception $e) {
          dd($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $configuraciones = Configuracion::find($id);
        $configuraciones->activo = 0;
        $configuraciones->save();



        return redirect()->route('configuraciones.index');
    }






}
