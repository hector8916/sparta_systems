<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Articulos;
use App\Categorias;
use \Milon\Barcode\DNS1D;
use \Milon\Barcode\DNS2D;  


class ArticuloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $datos['barra'] = new DNS1D();
       $datos['articulos_table'] = Articulos::where([['activo','1']])->orderBy('id','DESC')->get();

        return view('articulos.index')->with($datos);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $datos['categorias'] = Categorias::where('activo','1')->get();
        return view('articulos.create')->with($datos);        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            try {

                $articulos = new Articulos();
                $articulos->descripcion = $request->descripcion;
                $articulos->precio = $request->precio;
                $articulos->codigo = $request->codigo;
                $articulos->nombre = $request->nombre;
                $articulos->stock = $request->stock;
                $articulos->categoria = $request->categoria;
                $articulos->save();



                return response()->json(['success'=>'Se ha Agegado con Exito']);
            } catch (\Exception $e) {
              dd($e->getMessage());
            }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
     public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $datos['categorias'] = Categorias::where('activo','1')->get();
        $datos['articulos'] = Articulos::find($id);
        return view('articulos.create')->with($datos);    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        try {
            
            $articulos = Articulos::find($id);
            $articulos->fill($request->all());
            $articulos->save();
        
           return response()->json(['success'=>'Se ha Editado con Exito']);

        } catch (\Exception $e) {
          dd($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $articulos = Articulos::find($id);
        $articulos->activo = 0;
        $articulos->save();



        return redirect()->route('articulos.index');
    }






}
