<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Ventas;
use App\Ticket_Venta;
use App\Articulos;
use App\Vendedor;
use App\Tienda;

use \Milon\Barcode\DNS1D;
use \Milon\Barcode\DNS2D;

use Auth;

class VentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $datos['articulos'] = Articulos::where([['activo','1'],['stock','>=','1']])->get();
        return view('ventas.index')->with($datos);
      
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $usuario = Auth::user()->id_personal;
        
        $vendedor = Vendedor::find($usuario);
        $nombre_usuario = $vendedor->nombre;
        $tienda = $vendedor->id_tienda;
        $tiendas = Tienda::find($tienda);
        $data['sucursal'] = $tiendas->nombre;
        $data['sucursal_domicilio'] = $tiendas->domicilio;
        
        $data['usuario_atendio'] = $nombre_usuario;
        $data['venta'] = Ventas::where([['activo','1'],['id',$request->id_venta]])->get();
        $data['productos'] = Ticket_Venta::where([['activo','1'],['id_venta',$request->id_venta]])->get();

        return response()->json(['venta'=>$data]);


        //return view('ventas.create')->with($data);       
    }

    public function articulo_stock(Request $request){
        
        $articulos = Articulos::find($request->articulo_id);

        $data['stock'] = $articulos->stock;
        return response()->json(['stock'=>$data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usuario = Auth::user()->id_personal;
        

            try {

                $DesdeNumero = 1;
                $HastaNumero = 10000;
                $numeroAleatorio = rand($DesdeNumero, $HastaNumero);

                $venta = new Ventas();
                $venta->fecha = $request->hora;
                $venta->id_vendedor = $usuario;
                $venta->suma = $request->suma;
                $venta->cantidad = $request->cantidad;
                $venta->devolucion = $request->devolucion;
                $venta->monto_pago = $request->monto_pago;
                
                $venta->save();



                foreach ($request->productos as  $value) {
                    $ticket_venta = new Ticket_Venta();
                    $ticket_venta->id_venta = $venta->id;
                    $ticket_venta->cantidad = $value['cantidad'];
                    $ticket_venta->articulo_id = $value['articulo_id'];
                    $ticket_venta->articulo = $value['articulo'];
                    $ticket_venta->precio = $value['precio'];
                    $ticket_venta->codigo = $value['codigo'];
                    $ticket_venta->descripcion = $value['descripcion'];
                    $ticket_venta->save();


                    //////////////// articulo ////////////////////////
                    $articulo = Articulos::find($value['articulo_id']);
                    $resultado_stock = $articulo->stock - $value['cantidad'];

                    $articulo->stock = $resultado_stock;
                    $articulo->save();
                }


                ///////////// venta /////////////////////////////
                    $venta = Ventas::find($venta->id);
                    $venta->serial = '750'.$numeroAleatorio.$venta->id;
                    $venta->save();





                return response()->json(['success'=>'Su Compra ha sido un Exito','id'=>$venta->id]);
            } catch (\Exception $e) {
              dd($e->getMessage());
            }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        
        
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $datos['venta'] = Venta::find($id);
        return view('ventas.create')->with($datos);    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        try {
            
            $venta = Venta::find($id);
            $venta->fill($request->all());
            $venta->save();
        
           return response()->json(['success'=>'Se ha Editado con Exito']);

        } catch (\Exception $e) {
          dd($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $venta = Venta::find($id);
        $venta->activo = 0;
        $venta->save();



        return redirect()->route('ventas.index');
    }


    public function enviarcheck(Request $request){

        $data['articulos'] = Articulos::where([
          ['activo', '=', '1'],
          ['id', '=', $request->id],
        ])->get();

     return $data;

    }








}
