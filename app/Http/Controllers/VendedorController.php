<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Vendedor;
use App\User;
use App\Tienda;
use App\Permisos;

class VendedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       $datos['vendedor_table'] = Vendedor::where([['activo','1']])->orderBy('id','DESC')->get();

        return view('personal.index')->with($datos);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $datos['sucursal'] = Tienda::where('activo','1')->get();
        return view('personal.create')->with($datos);        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            try {

                $DesdeLetra = "a";
                $HastaLetra = "z";
                $DesdeNumero = 1;
                $HastaNumero = 10000;
                 
                $letraAleatoria = chr(rand(ord($DesdeLetra), ord($HastaLetra)));
                $numeroAleatorio = rand($DesdeNumero, $HastaNumero);

                $vendedor = new Vendedor();
                $vendedor->nombre = $request->nombre;
                $vendedor->apellido_paterno = $request->apellido_paterno;
                $vendedor->apellido_materno = $request->apellido_materno;
                $vendedor->id_tienda = $request->id_tienda;
                $vendedor->correo_electronico = $request['correo_electronico'];
                $vendedor->password = $letraAleatoria.$numeroAleatorio;
                $vendedor->save();


                $permisos = new Permisos();
                $permisos->id_usuario = $vendedor->id;
                $permisos->id_permisos = 2;
                $permisos->tipo_user = 3;
                $permisos->save();



                $usuario = new User();
                $usuario->name = $request['nombre'].'.'.$request['apellido_paterno'];
                $usuario->email = $request['correo_electronico'];
                $usuario->id_personal = $vendedor->id;
                $usuario->password = bcrypt($vendedor->password);
                $usuario->password_name = $vendedor->password;
                $usuario->tipo_user = 3;
                $tienda->imagen_usuario = '17004.png';
                $usuario->save();


                return response()->json(['success'=>'Se ha Agegado con Exito']);
            } catch (\Exception $e) {
              dd($e->getMessage());
            }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
     public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $datos['sucursal'] = Tienda::where('activo','1')->get();
        $datos['personal'] = Vendedor::find($id);
        return view('personal.create')->with($datos);    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        try {
            
            $vendedor = Vendedor::find($id);
            $vendedor->fill($request->all());
            $vendedor->save();
        
           return response()->json(['success'=>'Se ha Editado con Exito']);

        } catch (\Exception $e) {
          dd($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vendedor = Vendedor::find($id);
        $vendedor->activo = 0;
        $vendedor->save();

        User::where([['activo','1'],['id_personal',$id]])->update([
          "activo" => 0
        ]);


        return redirect()->route('personal.index');
    }






}
