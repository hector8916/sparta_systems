<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Descuentos;



class DescuentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       $datos['descuento_table'] = Descuentos::where([['activo','1']])->orderBy('id','DESC')->get();

        return view('descuentos.index')->with($datos);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('descuentos.create');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            try {

                $usuario = new Descuentos();
                $usuario->nombre_descuento = $request->nombre_descuento;
                $usuario->monto_descuento = $request->monto_descuento;
                $usuario->save();



                return response()->json(['success'=>'Se ha Agegado con Exito']);
            } catch (\Exception $e) {
              dd($e->getMessage());
            }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
     public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $datos['descuentos'] = Descuentos::find($id);
        return view('descuentos.create')->with($datos);    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        try {
            
            $user = Descuentos::find($id);
            $user->fill($request->all());
            $user->save();
        
           return response()->json(['success'=>'Se ha Editado con Exito']);

        } catch (\Exception $e) {
          dd($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Descuentos::find($id);
        $user->activo = 0;
        $user->save();



        return redirect()->route('descuentos.index');
    }






}
