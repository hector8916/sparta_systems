<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket_Venta extends Model
{
	protected $hidden = ['created_at','updated_at'];
    protected $fillable = ['id','id_venta','cantidad','articulo_id','articulo','precio','codigo','descripcion','activo'];
    protected $table = 'ticket_venta';

    
}