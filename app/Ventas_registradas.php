<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ventas_registradas extends Model
{
	protected $hidden = ['created_at','updated_at'];
    protected $fillable = ['id','idventa','idarticulo','descripcion','cantidad','precio','subtotal','total_comision','observaciones','activo'];
    protected $table = 'ventas_registro';

    
}
