<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Descuentos extends Model
{
	protected $hidden = ['created_at','updated_at'];
    protected $fillable = ['id','nombre_descuento','monto_descuento','activo'];
    protected $table = 'descuento';

    
}