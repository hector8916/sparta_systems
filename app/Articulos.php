<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articulos extends Model
{
	protected $hidden = ['created_at','updated_at'];
    protected $fillable = ['id','descripcion','precio','codigo','nombre','stock','categoria','activo'];
    protected $table = 'articulos';


    public function _Categoria(){
    return $this->hasOne('App\Categorias', 'id', 'categoria');
  	}

    
}

