<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorias extends Model
{
	protected $hidden = ['created_at','updated_at'];
    protected $fillable = ['id','nombre','activo'];
    protected $table = 'categorias';

    
}