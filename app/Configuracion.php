<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configuracion extends Model
{
	protected $hidden = ['created_at','updated_at'];
    protected $fillable = ['id','nombre_empresa','nombre_corto','direccion','cp','correo_electronico','telefono','logo_empresa','cambiar_skin;','activo'];
    protected $table = 'configuracion';


    
    
}