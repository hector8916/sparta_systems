<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendedor extends Model
{
	protected $hidden = ['created_at','updated_at'];
    protected $fillable = ['id','nombre','apellido_paterno','apellido_materno','correo_electronico','password','id_tienda','activo'];
    protected $table = 'vendedor';

    public function _Sucursal(){
    return $this->hasOne('App\Tienda', 'id', 'id_tienda');
  	}
}
