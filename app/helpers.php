<?php


// Helper de todos los usuarios por ejemplo
function users()
{
    return App\User::all();
}
// Helper de usuario autenticado (Logueado)
function authUser()
{
    return \Auth::user();
}

function configuracion()
{
    return App\Configuracion::all();
}

function permisos()
{
	$usuario = \Auth::user()->id_personal;

    return App\Permisos::where('id_usuario',$usuario)->get();
}

function menu()
{
	$usuario = \Auth::user()->id_personal;
		return App\Permisos::
	        join('menu', 'permisos.id_permisos', '=', 'menu.id')->
	        select(
	             'permisos.id_permisos',
	              'menu.nombre',
	              'menu.url'
	          )->where([
	          ['permisos.activo', '=', '1'],
	          ['menu.activo', '=', '1'],
	          ['permisos.id_usuario', '=', $usuario]])->get();
	
    //return App\Menu::where('activo','1')->get();
}