<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoUsuario extends Model
{
	protected $hidden = ['created_at','updated_at'];
    protected $fillable = ['id','nombre','activo'];
    protected $table = 'tipo_usuario';

    
}