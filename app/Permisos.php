<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permisos extends Model
{
	protected $hidden = ['created_at','updated_at'];
    protected $fillable = ['id','id_usuario','id_permios','tipo_user','activo'];
    protected $table = 'permisos';

    
}