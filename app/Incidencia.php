<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incidencia extends Model
{
	protected $hidden = ['created_at','updated_at'];
    protected $fillable = ['id','titulo_incidencia','tipo_incidencia','tipo_incidencia','observacion','imagen','activo'];
    protected $table = 'incidencias_sistema';

    
}