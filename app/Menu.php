<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
	protected $hidden = ['created_at','updated_at'];
    protected $fillable = ['id','nombre','url','activo'];
    protected $table = 'menu';

    
}