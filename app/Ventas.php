<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ventas extends Model
{
	protected $hidden = ['created_at','updated_at'];
    protected $fillable = ['id','fecha','id_vendedor','subtotal','iva','total','cambio','letratotal','efectivo','id_tienda','idatendido','observaciones','serial','activo'];
    protected $table = 'ventas';


    public function _operador(){
    return $this->hasOne('App\Vendedor', 'id', 'id_vendedor');
  	}
    
}
