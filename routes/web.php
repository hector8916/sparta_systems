<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



	 
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

	

	

    /*Todas las rutas que esten en este grupo estaran restringidas si nuestro aplicacion esta
	en mantenimiento y se restringira su paso si la pagina esta en mantenimiento.*/
	Route::get('/home', 'HomeController@index')->name('home');

	//Route::get('/', 'HomeController@index');
 	//////////// chat de usuarios //////////////
	Route::get('/load-latest-messages', 'MessagesController@getLoadLatestMessages');
 
	Route::post('/send', 'MessagesController@postSendMessage');
	 
	Route::get('/fetch-old-messages', 'MessagesController@getOldMessages');
	
	/////////////////////////////////articulos/////////////////////////
	Route::resource('articulos','ArticuloController');
	Route::post('/articulos/create', 'ArticuloController@store');
	Route::put('/{id}', 'ArticuloController@update');

	/////////////////////////////////tiendas/////////////////////////
	Route::resource('tiendas','TiendaController');
	Route::post('/tiendas/create', 'TiendaController@store');
	Route::put('/{id}', 'TiendaController@update');

	/////////////////////////////////vendedores/////////////////////////
	Route::resource('personal','VendedorController');
	Route::post('/personal/create', 'VendedorController@store');
	Route::put('/{id}', 'VendedorController@update');


	/////////////////////////////////clientes/////////////////////////
	Route::resource('clientes','ClienteController');
	Route::post('/clientes/create', 'ClienteController@store');
	Route::put('/{id}', 'ClienteController@update');

	/////////////////////////////////ventas/////////////////////////

	Route::resource('ventas','VentaController');

	Route::post('/ventas/create', 'VentaController@store');
	Route::post('/ventas/ticket_compra', 'VentaController@create');
	Route::post('/ventas/articulo', 'VentaController@articulo_stock');

	Route::put('/{id}', 'VentaController@update');
	//Route::post('/ventas/enviarcheck', 'VentaController@enviarcheck');
	Route::get('ventas/{id}/traerArticulo', 'VentaController@enviarcheck');

	/////////////////////////////////venta registradas/////////////////////////
	Route::resource('ventas_registradas','VentaregistroController');
	Route::post('/ventas_registradas/create', 'VentaregistroController@store');
	Route::put('/{id}', 'VentaregistroController@update');

	/////////////////////////////////categorias/////////////////////////
	Route::resource('categorias','CategoriaController');
	Route::post('/categorias/create', 'CategoriaController@store');
	Route::put('/{id}', 'CategoriaController@update');

	/////////////////////////////////tipo_usuarios/////////////////////////
	Route::resource('tipo_usuarios','TipoUsuarioController');
	Route::post('/tipo_usuarios/create', 'TipoUsuarioController@store');
	Route::put('/{id}', 'TipoUsuarioController@update');

	/////////////////////////////////usuarios/////////////////////////
	Route::resource('usuarios','UsuarioController');
	Route::post('/permisos', 'UsuarioController@permisos');
	Route::post('/borrar_permisos', 'UsuarioController@borrar_permisos');
	Route::post('/usuarios/create', 'UsuarioController@store');
	Route::put('/{id}', 'UsuarioController@update');
	

	/////////////////////////////////perfiles/////////////////////////
	Route::resource('perfiles','PerfilController');

	/////////////////////////////////chat/////////////////////////
	Route::resource('chats','ChatController');

	/////////////////////////////////descuentos/////////////////////////
	Route::resource('descuentos','DescuentoController');
	Route::post('/descuentos/create', 'DescuentoController@store');
	Route::put('/{id}', 'DescuentoController@update');

	/////////////////////////////////promociones/////////////////////////
	Route::resource('promociones','PromocionController');
	Route::post('/promociones/create', 'PromocionController@store');
	Route::put('/{id}', 'PromocionController@update');

	/////////////////////////////////configuraciones/////////////////////////
	Route::resource('configuraciones','ConfiguracionController');
	Route::post('/configuraciones/create', 'ConfiguracionController@store');
	Route::put('/{id}', 'ConfiguracionController@update');


	/////////////////////////////////configuraciones/////////////////////////
	Route::resource('incidencias','IncidenciaController');
	Route::post('/incidencias/create', 'IncidenciaController@store');
	Route::put('/{id}', 'IncidenciaController@update');

	///////////////////////// Cierrre de Sesión ///////////////
	Route::get('logout', function (){
		Auth::logout();
		return redirect('/');
	});







	




	


	



















