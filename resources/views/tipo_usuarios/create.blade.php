@extends('layouts.inicio')

@section('content')
<div class="col-md-12">
	<div class="box box-primary">
	    <div class="box-header with-border">
	    	@isset($tipo_usuarios)
	    	<h3 class="box-title">Actualizar Tipo Usuario</h3>
			@else
			<h3 class="box-title">Nuevo Tipo Usuario</h3>
			@endisset
	    </div>
	    <!-- /.box-header -->
	    <!-- form start -->
	    <form role="form">
	      <div class="box-body">
	      	<div class="form-group">
	          <label for="exampleInputPassword1">Tipo Usuario</label>
	          <input type="text" class="form-control" name="nombre" placeholder="Tipo de Usuario" value="@isset($tipo_usuarios){{ $tipo_usuarios->nombre }}@endisset">
	        </div>
	        
	      </div>
	      <!-- /.box-body -->

	      <div class="box-footer">
	      	<a href="{{ url('tipo_usuarios') }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Regresar</a>
	      	@isset($tipo_usuarios)
	      	<button type="submit" class="btn btn-primary btn-submit"><i class="fa fa-repeat"></i> Actualizar</button>
	      	@else
	        <button type="submit" class="btn btn-primary btn-submit"><i class="fa fa-save"></i> Agregar</button>
	        @endisset
	      </div>
	    </form>
	  </div>
</div> 

<script>
	$(document).ready( function () {


	    $(".btn-submit").click(function(e){

	    
        e.preventDefault();

        var nombre = $("input[name=nombre]").val();




        if(nombre == '' ){

        	
        	swal("Upss!", "Lo sentimos Campos Vacios", "warning");
        	

        }else{

        	$.ajax({

	           type:"{{ ( isset($tipo_usuarios) ? 'PUT' : 'POST' ) }}",

	           url:"{{ ( isset($tipo_usuarios) ) ? '/tipo_usuarios/' . $tipo_usuarios->id : '/tipo_usuarios/create' }}",
	           headers: {
				      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				     },
	           data:{
				nombre:nombre,
	           },
	           
	            success:function(data){
	                swal({title: "Felicidades!", text: data.success, type: "success"}, function(){ location.href ="{{ url('tipo_usuarios') }}"; } );
	            }


	        });

        }

  });
}); 
</script>
@endsection