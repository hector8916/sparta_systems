@extends('layouts.inicio')

@section('content')
<div class="col-md-12">
	<div class="box box-primary">
	    <div class="box-header with-border">
	    	@isset($personal)
	    	<h3 class="box-title">Actualizar Personal</h3>
			@else
			<h3 class="box-title">Nuevo Personal</h3>
			@endisset
	    </div>
	    <!-- /.box-header -->
	    <!-- form start -->
	    <form role="form">
	      <div class="box-body">
	      	<div class="col-md-6">
	      	<div class="form-group">
	          <label for="exampleInputPassword1">Nombre </label>
	          <input type="text" class="form-control" name="nombre" placeholder="Nombre" value="@isset($personal){{ $personal->nombre }}@endisset">
	        </div>
	    	</div>
	    	<div class="col-md-6">
		        <div class="form-group">
		          <label for="exampleInputEmail1">Apellido Paterno</label>
		          <input type="text" class="form-control" name="apellido_paterno" placeholder="Apellido Paterno" value="@isset($personal){{ $personal->apellido_paterno }}@endisset">
		        </div>
	    	</div>
	    	<div class="col-md-6">
		        <div class="form-group">
		          <label for="exampleInputPassword1">Apellido Materno</label>
		          <input type="text" class="form-control" name="apellido_materno" placeholder="Apellido Materno" value="@isset($personal){{ $personal->apellido_materno }}@endisset">
		        </div>
		    </div>
	        <div class="col-md-6">
		        <div class="form-group">
		          <label for="exampleInputPassword1">Correo Electronico</label>
		          <input type="text" class="form-control" name="correo_electronico" placeholder="Correo Electronico" value="@isset($personal){{ $personal->correo_electronico }}@endisset">
		        </div>
		    </div>
	        <div class="col-md-6">
              	<div class="form-group">
	                <label>Sucursal</label>
	                <select class="form-control select2 select2-hidden-accessible" style="width: 100%;"  name="id_tienda" tabindex="-1" aria-hidden="true">
	                 @isset($personal)
	                 <option value="{{ $personal->id_tienda }}">{{ $personal->_Sucursal->nombre }}</option>
	                 @else
	                 <option>Selecciona una Categoria</option>
	                 @endisset
	                  @foreach($sucursal as $tienda)
	                  <option value="{{ $tienda->id }}">{{ $tienda->nombre }}</option>
	                  @endforeach
	                </select>
            	</div>
              
            </div>
	        
	      </div>
	      <!-- /.box-body -->

	      <div class="box-footer">
	      	<a href="{{ url('personal') }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Regresar</a>
	      	@isset($personal)
	      	<button type="submit" class="btn btn-primary btn-submit"><i class="fa fa-repeat"></i> Actualizar</button>
	      	@else
	        <button type="submit" class="btn btn-primary btn-submit"><i class="fa fa-save"></i> Agregar</button>
	        @endisset
	      </div>
	    </form>
	  </div>
</div>

<script>
	$(document).ready( function () {


	    $(".btn-submit").click(function(e){

	    
        e.preventDefault();

        var nombre = $("input[name=nombre]").val();
        var apellido_paterno = $("input[name=apellido_paterno]").val();
        var apellido_materno = $("input[name=apellido_materno]").val();
        var correo_electronico = $("input[name=correo_electronico]").val();
        var id_tienda = $("select[name=id_tienda]").val();



        if(nombre == '' || apellido_paterno == '' || apellido_materno == '' || correo_electronico == ''){

        	
        	swal("Upss!", "Lo sentimos Campos Vacios", "warning");
        	

        }else{

        	$.ajax({

	           type:"{{ ( isset($personal) ? 'PUT' : 'POST' ) }}",

	           url:"{{ ( isset($personal) ) ? '/personal/' . $personal->id : '/personal/create' }}",
	           headers: {
				      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				     },
	           data:{
	           	nombre:nombre,
				apellido_paterno:apellido_paterno,
				apellido_materno:apellido_materno,
				correo_electronico:correo_electronico,
				id_tienda:id_tienda,

				
	           },
	           
	            success:function(data){
	                swal({title: "Felicidades!", text: data.success, type: "success"}, function(){ location.href ="{{ url('personal') }}"; } );
	            }


	        });

        }

  });
}); 
</script>
@endsection