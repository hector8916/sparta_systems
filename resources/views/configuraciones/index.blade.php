@extends('layouts.inicio')

@section('content')
<div class="col-md-12">
  <!-- MAP & BOX PANE -->
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title"><img class="img-circle" src="../images/<?php list($confi) = configuracion(); print_r($confi->logo_empresa);?>" alt="User Avatar" heigth="30" width="30"> Configuración <?php list($confi) = configuracion(); print_r($confi->nombre_empresa);?> </h3>

      <div class="box-tools pull-right">
        <a href="{{ route('configuraciones.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Nueva Configuración</a>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding">

    	  <table id="example" class="display" style="width:100%">
	        <thead>
	          <tr align="center">
              <th>Nombre </th>
	            <th>Nombre Corto</th>
	            <!--<th>Direccioón</th>-->
	            <!--<th>Codigo Postal</th>-->
              <th>Correo Electronico</th>
              <th>Telefono</th>
              <th>logo Empresa</th>
              <th>Color de Tema</th>
	            <th>Acciones</th>
	          </tr>
	        </thead>
	        <tbody>
	          @foreach ($configuraciones_table as $configuracion)
                  <tr align="center">
                      <td>{{ $configuracion->nombre_empresa }}</td>
                      <td>{{ $configuracion->nombre_corto }}</td>
                      <!--<td>{{ $configuracion->direccion }}</td>-->
                      <!--<td>{{ $configuracion->cp }}</td>-->
                      <td>{{ $configuracion->correo_electronico }}</td>
                      <td>{{ $configuracion->telefono }}</td>
                      <td><img src="../../images/<?php echo $configuracion->logo_empresa; ?>" heigth="50" width="50"></td>
                      <td>
                        <?php 
                           if ($configuracion->cambiar_skin == 'skin-blue') {
                              echo 'Color Azul';
                            }elseif($configuracion->cambiar_skin == 'skin-green'){
                              echo 'Color Verde';
                            }elseif($configuracion->cambiar_skin == 'skin-purple'){
                              echo 'Color Morado';  
                            }elseif($configuracion->cambiar_skin == 'skin-red'){
                              echo 'Color Rojo';
                            }elseif($configuracion->cambiar_skin == 'skin-yellow'){
                              echo 'Color Amarillo';
                            }
                        ?>
                      </td>

                      <td >
                        <form action="{{ route('configuraciones.destroy',$configuracion->id )}}" method="POST">
                                      @csrf
                                      @method('DELETE')
                        <div class="btn-group">
                          <button type="button" class="btn btn-warning "><a href="{{ route('configuraciones.edit',$configuracion->id)}}" style="color:#fff;"><i class="fa fa-pencil"></i> Editar</a></button>
                            <button class="btn btn-danger" type="submit"><i class="fa fa-trash"></i> Eliminar</button>
                        </div>
                        </form>
                        
                      </td>
                  </tr>
                @endforeach
	        </tbody>
	      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->

</div>
<script>
  $(document).ready(function() {
      
      $('#example').DataTable({
    
    "scrollY": 300,
    "scrollX": true,
    "pagingType": "full_numbers",
   language: {
    processing:     "Tratamiento en curso...",
    search:         "Buscar&nbsp;:",
    lengthMenu:    "Mostrando _MENU_ registros por página",
    info:           "Mostrando página _START_ a _TOTAL_ Elementos",
    infoEmpty:      "No hay registros disponibles",
    infoFiltered:   "(Filtrado de _MAX_ registros totales)",
    infoPostFix:    "",
    loadingRecords: "Cargando...",
    zeroRecords:    "Ningun Resgistro",
    emptyTable:     "No hay datos disponibles por el momento.",
    paginate: {
        first:      "Primeramente",
        previous:   "Anteriormente",
        next:       "Siguiente",
        last:       "Ultimo"
    },
    aria: {
        sortAscending:  ": Activar para ordenar la columna en orden ascendente.",
        sortDescending: ": Activar para ordenar la columna en orden descendente."
    }
},
dom: 'B<"clear">lfrtip',
    buttons: [
        'colvis'
    ],
});
      
  } );
</script>
@endsection