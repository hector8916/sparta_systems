@extends('layouts.inicio')

@section('content')
<div class="col-md-12">
	<div class="box box-primary">
	    <div class="box-header with-border">
	    	@isset($configuraciones)
	    	<h3 class="box-title">Actualizar configuracion</h3>
			@else
			<h3 class="box-title">Nuevo configuracion</h3>
			@endisset
	    </div>
	    <!-- /.box-header -->
	    <!-- form start -->
	    @isset($configuraciones)
	        <form action="{{ route('configuraciones.update',$configuraciones->id) }}" method="POST" enctype="multipart/form-data">
	        @csrf
	        @method('PUT')
	      @else
	        <form action="{{ route('configuraciones.store') }}" method="POST" enctype="multipart/form-data">
	        @csrf
	      @endisset
	      <div class="box-body">
	      	<div class="col-md-4">
	      	<div class="form-group">
	          <label for="exampleInputPassword1">Nombre de la Empresa</label>
	          <input type="text" class="form-control" name="nombre_empresa" placeholder="Nombre de la Empresa" value="@isset($configuraciones){{ $configuraciones->nombre_empresa }}@endisset">
	        </div>
	    	</div>
	    	<div class="col-md-4">
		        <div class="form-group">
		          <label for="exampleInputEmail1">Nombre Corto</label>
		          <input type="text" class="form-control" name="nombre_corto" placeholder="Nombre Corto" value="@isset($configuraciones){{ $configuraciones->nombre_corto }}@endisset">
		        </div>
	    	</div>
	    	<div class="col-md-4">
		        <div class="form-group">
		          <label for="exampleInputPassword1">Dirección</label>
		          <input type="text" class="form-control" name="direccion" placeholder="Dirección" value="@isset($configuraciones){{ $configuraciones->direccion }}@endisset">
		        </div>
		    </div>
		    <div class="col-md-4">
		        <div class="form-group">
		          <label for="exampleInputPassword1">Codigo Postal</label>
		          <input type="text" class="form-control" name="cp" placeholder="Codigo Postal" value="@isset($configuraciones){{ $configuraciones->cp }}@endisset">
		        </div>
	        </div>
	        <div class="col-md-4">
		        <div class="form-group">
		          <label for="exampleInputPassword1">Correo Electronico</label>
		          <input type="email" class="form-control" name="correo_electronico" placeholder="Correo Electronico" value="@isset($configuraciones){{ $configuraciones->correo_electronico }}@endisset">
		        </div>
		    </div>

		    <div class="col-md-4">
		        <div class="form-group">
		          <label for="exampleInputPassword1">Telefono</label>
		          <input type="text" class="form-control" name="telefono" placeholder="Telefono" value="@isset($configuraciones){{ $configuraciones->telefono }}@endisset">
		        </div>
		    </div>


		    <div class="col-md-4">
              	<div class="form-group">
	                <label>Color Skin</label>
	                <select class="form-control select2 select2-hidden-accessible" style="width: 100%;"  name="cambiar_skin" tabindex="-1" aria-hidden="true">
	                 @isset($configuraciones)
	                 <option value="{{ $configuraciones->cambiar_skin }}">
	                 	<?php 
	                 		if ($configuraciones->cambiar_skin == 'skin-blue') {
	                 			echo 'Color Azul';
	                 		}elseif($configuraciones->cambiar_skin == 'skin-green'){
	                 			echo 'Color Verde';
	                 		}elseif($configuraciones->cambiar_skin == 'skin-purple'){
	                 			echo 'Color Morado';	
	                 		}elseif($configuraciones->cambiar_skin == 'skin-red'){
	                 			echo 'Color Rojo';
	                 		}elseif($configuraciones->cambiar_skin == 'skin-yellow'){
	                 			echo 'Color Amarillo';
	                 		}
	                 	 ?>
	                 </option>
	                 @else
	                 <option>Selecciona un Color</option>
	                 @endisset
	                  <option value="skin-blue">Color Azul</option>
	                  <option value="skin-green">Color Verde</option>
	                  <option value="skin-purple">Color Morado</option>
	                  <option value="skin-red">Color Rojo</option>
	                  <option value="skin-yellow">Color Amarillo</option>

	                </select>
            	</div>
              
            </div>

		    @isset($configuraciones)
            <div class="col-md-4">
              	<div class="form-group">
	                <label>Logo Empresa</label>
	                <div style="height: 20px;"></div>
	                <img src="../../images/{{ $configuraciones->logo_empresa }}" alt="" width="120" height="120">
	                <br>
	                <label>Cambiar Imagen</label>
                    <input type="file"  name="avatar" value="">
	            </div>
	        </div>

            
            @else
            <div class="col-md-4">
		        <div class="form-group">
		          <label for="exampleInputPassword1">Logo Empresa</label>
		          <input type="file"  name="avatar" value="">
		        </div>
		    </div>
		    @endisset


	        
	      </div>
	      <!-- /.box-body -->

	      <div class="box-footer">
	      	<a href="{{ url('configuraciones') }}" class="btn btn-warning"> <i class="fa fa-arrow-left"></i> Regresar</a>
	      	@isset($configuraciones)
	      	<button type="submit" class="btn btn-primary btn-submit"><i class="fa fa-repeat"></i> Actualizar</button>
	      	@else
	        <button type="submit" class="btn btn-primary btn-submit"><i class="fa fa-save"></i> Agregar</button>
	        @endisset
	      </div>
	    </form>
	  </div>
</div>

<!--<script>
	$(document).ready( function () {


	    $(".btn-submit").click(function(e){

	    
        e.preventDefault();

        var nombre_empresa = $("input[name=nombre_empresa]").val();
        var nombre_corto = $("input[name=nombre_corto]").val();
        var direccion = $("input[name=direccion]").val();
        var cp = $("input[name=cp]").val();
        var correo_electronico = $("input[name=correo_electronico]").val();
        var telefono = $("input[name=telefono]").val();
        var logo_empresa = $("input[name=logo_empresa]").val();
        var imagen_login = $("input[name=imagen_login]").val();





        if(nombre_empresa == '' || correo_electronico == ''){

        	
        	swal("Upss!", "Lo sentimos Campos Vacios", "warning");
        	

        }else{

        	$.ajax({

	           type:"{{ ( isset($configuraciones) ? 'PUT' : 'POST' ) }}",

	           url:"{{ ( isset($configuraciones) ) ? '/configuraciones/' . $configuraciones->id : '/configuraciones/create' }}",
	           headers: {
				      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				     },
	           data:{
	           	nombre_empresa:nombre_empresa,
				nombre_corto:nombre_corto,
				direccion:direccion,
				cp:cp,
				correo_electronico:correo_electronico,
				telefono:telefono,
				logo_empresa:logo_empresa,
				imagen_login:imagen_login,
				
	           },
	           
	            success:function(data){
	                swal({title: "Felicidades!", text: data.success, type: "success"}, function(){ location.href ="{{ url('configuraciones') }}"; } );
	            }


	        });

        }

  });
}); 
</script>-->
@endsection