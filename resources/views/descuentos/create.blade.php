@extends('layouts.inicio')

@section('content')
<div class="col-md-12">
	<div class="box box-primary">
	    <div class="box-header with-border">
	    	@isset($descuentos)
	    	<h3 class="box-title">Actualizar Descuento</h3>
			@else
			<h3 class="box-title">Nuevo Descuento</h3>
			@endisset
	    </div>
	    <!-- /.box-header -->
	    <!-- form start -->
	    <form role="form">
	      <div class="box-body">
	      	<div class="form-group">
	          <label for="exampleInputPassword1">Nombre Descuento</label>
	          <input type="text" class="form-control" name="nombre_descuento" placeholder="Nombre Descuento" value="@isset($descuentos){{ $descuentos->nombre_descuento }}@endisset">
	        </div>

	        <div class="form-group">
	          <label for="exampleInputPassword1">Monto Descuento</label>
	          <input type="text" class="form-control" name="monto_descuento" placeholder="Monto Descuento" value="@isset($descuentos){{ $descuentos->monto_descuento }}@endisset">
	        </div>
	      </div>
	      <!-- /.box-body -->

	      <div class="box-footer">
	      	<a href="{{ url('descuentos') }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Regresar</a>
	      	@isset($descuentos)
	      	<button type="submit" class="btn btn-primary btn-submit"><i class="fa fa-repeat"></i> Actualizar</button>
	      	@else
	        <button type="submit" class="btn btn-primary btn-submit"><i class="fa fa-save"></i> Agregar</button>
	        @endisset
	      </div>
	    </form>
	  </div>
</div>

<script>
	$(document).ready( function () {


	    $(".btn-submit").click(function(e){

	    
        e.preventDefault();

        var nombre_descuento = $("input[name=nombre_descuento]").val();
        var monto_descuento = $("input[name=monto_descuento]").val();




        if(nombre_descuento == '' || monto_descuento == '' ){

        	
        	swal("Upss!", "Lo sentimos Campos Vacios", "warning");
        	

        }else{

        	$.ajax({

	           type:"{{ ( isset($descuentos) ? 'PUT' : 'POST' ) }}",

	           url:"{{ ( isset($descuentos) ) ? '/descuentos/' . $descuentos->id : '/descuentos/create' }}",
	           headers: {
				      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				     },
	           data:{
				nombre_descuento:nombre_descuento,
				monto_descuento:monto_descuento,
	           },
	           
	            success:function(data){
	                swal({title: "Felicidades!", text: data.success, type: "success"}, function(){ location.href ="{{ url('descuentos') }}"; } );
	            }


	        });

        }

  });
}); 
</script>
@endsection