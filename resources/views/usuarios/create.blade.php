@extends('layouts.inicio')

@section('content')
<style type="text/css" media="screen">
.select2-container--default .select2-selection--multiple .select2-selection__choice {
background-color: 
#3c8dbc;
border-color:
#367fa9;
padding: 1px 10px;
color:
    #fff;
}
.my-custom-scrollbar {
position: relative;
height: 200px;
overflow: auto;
}
.table-wrapper-scroll-y {
display: block;
}
</style>
<div class="col-md-12">
	<div class="box box-primary">
	    <div class="box-header with-border">
	    	@isset($usuarios)
	    	<h3 class="box-title">Actualizar Usuario</h3>
			@else
			<h3 class="box-title">Nuevo Usuario</h3>
			@endisset
	    </div>
	    <!-- /.box-header -->
	    <!-- form start -->

	    @isset($usuarios)
	        <form action="{{ route('usuarios.update',$usuarios->id) }}" method="POST" enctype="multipart/form-data">
	        @csrf
	        @method('PUT')
	      @else
	        <form action="{{ route('usuarios.store') }}" method="POST" enctype="multipart/form-data">
	        @csrf
	      @endisset
	      <div class="box-body">
	      	<div class="col-md-6">
	      	<div class="form-group">
	          <label for="exampleInputPassword1">Nombre del Usuario</label>
	          <input type="text" class="form-control" name="name" placeholder="Nombre de Articulo" value="@isset($usuarios){{ $usuarios->name }}@endisset">
	        </div>
	    	</div>
	    	<div class="col-md-6">
		        <div class="form-group">
		          <label for="exampleInputEmail1">Correo Electronico</label>
		          <input type="text" class="form-control" name="email" placeholder="Correo Electronico" value="@isset($usuarios){{ $usuarios->email }}@endisset">
		        </div>
	    	</div>
	    	<div class="col-md-6">
		        <div class="form-group">
		          <label for="exampleInputPassword1">Password</label>
		          <input type="text" class="form-control" name="password" placeholder="Password" value="@isset($usuarios){{ $usuarios->password_name }}@endisset">
		        </div>
		    </div>

		    

	        <div class="col-md-6">
              	<div class="form-group">
	                <label>Tipo de Usuario</label>
	                <select class="form-control select2 select2-hidden-accessible" style="width: 100%;"  name="tipo_user" tabindex="-1" aria-hidden="true">
	                 @isset($usuarios)
	                	 <option value="{{ $usuarios->tipo_user }}">{{ $usuarios->_TipoUsuario->nombre }}</option>
	                 @else
	                 <option>Selecciona una Categoria</option>
	                 @endisset
	                  @foreach($tipo_usuario as $usuarios_tipo)
	                  <option value="{{ $usuarios_tipo->id }}">{{ $usuarios_tipo->nombre }}</option>
	                  @endforeach
	                </select>
            	</div>
              
            </div>
            
            <div class="col-md-12">

            	<div class="col-md-6 ">
	              	<div class="form-group">
		                <label>Permisos</label>
		                <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" id="id_permiso" name="id_permiso" tabindex="-1" aria-hidden="true">
		                 <option value="0">Selecciona un Permiso</option>
		                  @foreach($menu as $permiso)
		                  <option value="{{ $permiso->id }}">{{ $permiso->nombre }}</option>
		                  @endforeach
		                </select>
	            	</div>
	              	<br>
	              	<button type="button" class="btn btn-primary pull-right" id="agregarArticulo"><i class="fa fa-check"></i> Agregar</button>


	              	<div class="row">
				        <div class="col-xs-12 table-responsive">
				        	<label>Tabla Permisos de Usuario</label>
				        <div class="table-wrapper-scroll-y my-custom-scrollbar">

		  				<table class="table table-bordered table-striped mb-0" id="tablaArticulo" style=" text-align: center; ">

				            <thead>
				            <tr >
				              <th>Permiso</th>
				              <th>Accion</th>
				            </tr>
				            </thead>
				            <tbody>
				            	@isset($permisos)
				            		@foreach($permisos as $key => $permiso)
				            		<input type="hidden" id="objeto" value="{{ json_encode($key) }}">
				            			<tr id="filas{{ $permiso->id }}">
				            				<td>{{ $permiso->nombre }}</td>
				            				<td><button type="button" class="btn btn-danger" onclick="eliminarMayor({{ $permiso->id }})" ><i class="fa fa-trash"></i></button></td>
				            			</tr>
				            		@endforeach
				            	@endisset
				            </tbody>
				          </table>
				          </div>
				        </div>
				        <!-- /.col -->
				      </div>
				      
			        	<button type="button" class="btn btn-primary btn-submit pull-right"><i class="fa fa-check"></i> Agregar Permisos</button>

            	</div>


            	<div class="col-md-6">

	        		 @isset($usuarios)
		            <div class="col-md-6">
		              	<div class="form-group">
			                <label>Imagen de Usuario</label>
			                <div style="height: 20px;"></div>
			                <img src="../../images/{{ $usuarios->imagen_usuario }}" alt="" width="120" height="120">
			                <br>
			                <label>Cambiar Imagen</label>
		                    <input type="file"  name="avatar" value="">
			            </div>
			        </div>


		            
		            @else
		            <div class="col-md-6">
				        <div class="form-group">
				          <label for="exampleInputPassword1">Imagen del Usuario</label>
				          <input type="file"  name="avatar" value="">
				        </div>
				    </div>
				    @endisset
            	</div>

            </div>

		      

		      <!-- /.row -->
		    


           
            
            
            
		    
            @isset($usuarios)
            <input type="hidden" name="id_personal" value="@isset($usuarios){{ $usuarios->id_personal }}@endisset">
            <input type="hidden" name="tipo_user_2" value="@isset($usuarios){{ $usuarios->tipo_user }}@endisset">
            <input type="hidden" name="id" value="@isset($usuarios){{ $usuarios->id }}@endisset">
	        @endisset
	      </div>
	      <!-- /.box-body -->

	      <div class="box-footer">
	      	<a href="{{ url('usuarios') }}" ><button type="button" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Regresar</button></a>
	      	@isset($usuarios)
	      	<button type="submit" class="btn btn-primary "><i class="fa fa-repeat"></i> Actualizar</button>
	      	@else
	        <button type="submit" class="btn btn-primary "><i class="fa fa-save"></i> Agregar</button>
	        @endisset
	      </div>
	    </form>
	  </div>
</div>



<script>
	var arrayAsp = [];
    
  		var objAsp = {};
	$(document).ready( function () {

		


	$(document).on("click","#agregarArticulo",function(e){
      	e.preventDefault();

     
      	
      	var permiso_id = $('#id_permiso').val();
      	console.log(permiso_id);
      	var permiso = $('select[name="id_permiso"] option:selected').text();


      	if (permiso_id == 0 ) {
                swal({title: "Cuidado!", text: "campos vacios", type: "warning"}, function(){ } );
	     
	     }else{

	     	objAsp = {
                      
                      permiso_id : $('#id_permiso').val(),
                      permiso: $('select[name="id_permiso"] option:selected').text(),
                    }


                    agregarAsp(objAsp);
                    arrayAsp.push(objAsp);

	      }



  		});

		




		$(".btn-submit").click(function(e){
        e.preventDefault();
        ////////// limpieza

        var id_personal = $('input[name=id_personal]').val();
        var tipo_user = $('input[name=tipo_user_2]').val();


        if(arrayAsp == 0){
        	swal({title: "Cuidado!", text: "No Existen Permisos Nuevos", type: "warning"}, function(){ } );
        }else{
        	$.ajax({

	           type:"POST",

	           url:"/permisos",
	           headers: {
				      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				     },
	           data:{
	           	tipo_user:tipo_user,
	           	id_personal:id_personal,
				permisos:arrayAsp,
	           },
	           
	            success:function(data){
	                swal({title: "Felicidades!", text: data.success, type: "success"}, function(){  } );
	            }


	        });
        }


        	

       
        });
	    
}); 


	var contador_asp = 0;
	function agregarAsp(objAsp){
	  //construccion de tabla
	    var tr = '<tr class="borrar_aspc_create" id="filas'+contador_asp+'">'+
	    
	    '<td>'+objAsp.permiso+'<input type="hidden" name="id_materia[]" value="'+objAsp.permiso_id+'" id="nvel_educativo_nombre'+contador_asp+'"/></td>'+
	    '<td style=" text-align: center; "><div  class="btn btn-danger" onclick="eliminarAsp_create('+contador_asp+')"> <i class="fa fa-trash"></i></div></td>'
	    '</tr>';




      
	    $("#tablaArticulo").append(tr);
	    //limpiar campos

	    $('#id_permiso').val('');
	    
	 contador_asp ++;
   //////////////////////////////////////
                    
	}

//$(document).on("click",".borrar_figura",function(e){
//      var id_figura_nueva = $(this).attr('figura_nueva_id');
//      e.preventDefault();
//      var id = $('#figura_nueva').val();
//      arrayFiguras.splice(id, 1);
//      $('#id-figura-'+id_figura_nueva).remove();
//  });

	function eliminarAsp_create(id) {
	    console.log(id);
	    
	    arrayAsp.splice(id,1);

	      $('#filas'+id).remove();
	    }


///////////////////////////////
        function eliminarMayor(id) {

        	console.log()
            var objeto = $('#objeto').val();

            eval(objeto);
            arrayAsp = JSON.parse(objeto);
            

            $.ajax({
              url: "/borrar_permisos",
              type: "POST",
              headers:  {'X-CSRF-TOKEN':$("meta[name='csrf-token']").attr('content')},
              data: {id: id}
            });

            $('#filas'+id).remove();

        }


</script>
@endsection