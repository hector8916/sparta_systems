@extends('layouts.inicio')

@section('content')
    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <?php list($confi) = configuracion(); print_r($confi->nombre_empresa);?>
            <small class="pull-right">Fecha: <?php echo date('Y-m-d') ?>, Hora:<?php date_default_timezone_set('America/Mexico_City');echo date('H:i') ?></small>
            <input type="hidden" value="<?php echo date('Y-m-d h:i:s') ?>" name="hora">
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">



        <!-- /.col -->
      </div>
      <!-- /.row -->
      <hr>
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped " id="tablaArticulo">
            <thead>
            <tr align="center">
              <th>Cantidad</th>
              <th>Producto</th>
              <th>Serial #</th>
              <th>Description</th>
              <th>Subtotal</th>
              <th>Accion</th>
            </tr>
            </thead>
            <tbody>
              @foreach($articulos as $value)
              <tr align="center">
                <td>{{ $value->cantidad }}</td>
                <td>{{ $value->articulo }}</td>
                <td>{{ $value->codigo }}</td>
                <td>{{ $value->descripcion }}</td>
                <td>{{ $value->precio }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <hr>
      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead" style="visibility: hidden;">Métodos de pago:</p>
          <div class="table-responsive">
            <table class="table">
              <tr>
                <th>Tipo de Cambio:</th>
                <td>
                  <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <input type="text" class="form-control" name="monto_pago" id="monto_pago" value="{{ $ventas->monto_pago }}">
                    <span class="input-group-addon">.00</span>
                  </div>
                </td>
              </tr>
              <tr>
                <th>Cambio: $</th>
                <td>
                  <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <input type="text" class="form-control" name="devolucion" id="devolucion" value="{{ $ventas->devolucion }}">
                    <span class="input-group-addon">.00</span>
                  </div>
                </td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead">Monto de la Venta</p>

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th>Cantidad:</th>
                <td>
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fas fa-shopping-bag"></i></span>
                    <input type="text" class="form-control" name="cantidad" value="{{ $ventas->cantidad }}" disabled>
                  </div>
                </td>
              </tr>
              <tr>
                <th>Total: $</th>
                <td >
                  <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <input type="text" class="form-control" name="suma" id="suma" value="{{ $ventas->suma }}">
                    <span class="input-group-addon">.00</span>
                  </div>
                </td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <div class="btn-submit btn btn-primary pull-right"><i class="fas fa-cash-register"></i> Pagar</div>
        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>




<script>
  var arrayAsp = [];
    
  var objAsp = {};

  var arrayEspaciolact = [];

///////////////////////////////////////////////////////////////////////////////////////

 $(document).on("click","#agregarArticulo",function(e){
      e.preventDefault();

     
      var cantidad = $('#id_cantidad').val();
      var articulo_id = $('#id_articulo').val();
      var articulo = $('select[name="id_articulo"] option:selected').text();
      

      if (cantidad == '' || articulo_id == '' ) {
                swal({title: "Cuidado!", text: "campos vacios", type: "warning"}, function(){ } );
      }else{
        
      $.ajax({
        url: 'ventas/'+articulo_id+'/traerArticulo',
        type: "GET",
        headers:  {'X-CSRF-TOKEN':$("meta[name='csrf-token']").attr('content')},
        dataType:'json',
          success:function(data)
          {

            otrosSQL = data.articulos;
            for($a = 0; $a < otrosSQL.length; $a++) {

              var precio = otrosSQL[$a].precio;
              var codigo = otrosSQL[$a].codigo;
              var descripcion = otrosSQL[$a].descripcion;


             var precio_subtotal =  parseFloat(otrosSQL[$a].precio) * parseFloat(cantidad);

                    
                    objAsp = {
                      cantidad: $('#id_cantidad').val(),
                      articulo_id : $('#id_articulo').val(),
                      articulo: $('select[name="id_articulo"] option:selected').text(),
                      precio : precio_subtotal,
                      codigo : codigo,
                      descripcion : descripcion,

                    }


                    agregarAsp(objAsp);
                    arrayAsp.push(objAsp);

                    
                    ///////////// suma de total ////////////
                    var numero =[];
                    var suma = 0;

                    var numero_ca =[];
                    var suma_ca = 0;


                     for($o = 0; $o < arrayAsp.length; $o++) {

                          numero[$o] = arrayAsp[$o].precio;
                          suma = suma + numero[$o];


                          numero_ca[$o] = arrayAsp[$o].cantidad;
                          suma_ca =   parseFloat(numero_ca[$o]) + parseFloat(suma_ca);
                         

                          
                       }

                       $('#total_productos').html('<div class="input-group">'+
                                                  '<span class="input-group-addon">$</span>'+
                                                  '<input type="text" class="form-control" name="suma" value="'+suma+'" disabled>'+
                                                  '<span class="input-group-addon">.00</span>'+
                                                  '</div>');

                       $('#total_cantidad').html('<div class="input-group">'+
                                                  '<span class="input-group-addon"><i class="fas fa-shopping-bag"></i></span>'+
                                                  '<input type="text" class="form-control" name="cantidad" value="'+suma_ca+'" disabled>'+
                                                  '</div>');


                       $("#monto_pago").change(function(){
                        

                          var devolucion_monto =  parseFloat($('#monto_pago').val() - parseFloat(suma) );

                          $('#devolucion').html('<div class="input-group">'+
                                                  '<span class="input-group-addon">$</span>'+
                                                  '<input type="text" class="form-control" name="devolucion" value="'+devolucion_monto+'" disabled>'+
                                                  '<span class="input-group-addon">.00</span>'+
                                                  '</div>');
                        });




              

              
              
            }
            
          }
      });

    }


      
  });
 var contador_asp = 5;
	function agregarAsp(objAsp){
	  //construccion de tabla
	    var tr = '<tr class="borrar_aspc_create" id="filas'+contador_asp+'">'+
	    
	    '<td>'+objAsp.cantidad+'<input type="hidden" name="id_materia[]" value="'+objAsp.cantidad+'" id="nvel_educativo_nombre'+contador_asp+'"/></td>'+
	    '<td>'+objAsp.articulo+'<input type="hidden" name="id_grupo[]" value="'+objAsp.articulo+'" id="nvel_educativo_nombre'+contador_asp+'"/></td>'+
      '<td>'+objAsp.codigo+'<input type="hidden" name="id_grupo[]" value="'+objAsp.codigo+'" id="nvel_educativo_nombre'+contador_asp+'"/></td>'+
      '<td>'+objAsp.descripcion+'<input type="hidden" name="id_grupo[]" value="'+objAsp.descripcion+'" id="nvel_educativo_nombre'+contador_asp+'"/></td>'+
      '<td>$'+objAsp.precio+'<input type="hidden" name="id_grupo[]" value="'+objAsp.precio+'" id="precios_total'+contador_asp+'"/></td>'+

	    '<td style=" text-align: center; "><div  class="btn btn-danger" onclick="eliminarAsp_create('+contador_asp+')"> <i class="fas fa-trash-alt"></i></div></td>'
	    '</tr>';

      
	    $("#tablaArticulo").append(tr);
	    //limpiar campos
	    $('#id_cantidad').val('');
	    $('#id_articulo').val('');
	    
	 contador_asp ++;
   //////////////////////////////////////



   
                    
	}

	function eliminarAsp_create(id) {
	    
	    
	    arrayAsp.splice(id,1);

	      $('#filas'+id).remove();
	    }


///////////////////////////////////////////



        $(".btn-submit").click(function(e){
          e.preventDefault();
            

            var suma = $("input[name=suma]").val();
            var cantidad = $("input[name=cantidad]").val();
            var devolucion = $("input[name=devolucion]").val();
            var monto_pago = $("input[name=monto_pago]").val();
            var hora = $("input[name=hora]").val();



            if(monto_pago == 0){
              swal({title: "Cuidado!", text: "El Ingresar el monto de pago", type: "warning"}, function(){ } );
            }else{
              $.ajax({

               type:"POST",
               url:"/ventas/create",
               headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
               data:{
                hora:hora,
                suma:suma,
                cantidad:cantidad,
                devolucion:devolucion,
                monto_pago:monto_pago,
                productos:arrayAsp
               },
               
                success:function(data){
                    swal({title: "Felicidades!", text: data.success, type: "success"}, function(){ location.href ="{{ url('ventas') }}"; } );

                    


                }


            });
              ////////////// ticket compra /////////////////////////////
              $.ajax({

               type:"POST",
               url:"/ventas/ticket_compra",
               headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
               data:{
                hora:hora,
                suma:suma,
                cantidad:cantidad,
                devolucion:devolucion,
                monto_pago:monto_pago,
                productos:arrayAsp
               },
               
                success:function(data){
                  window.open('{{ route("ventas.create") }}', '_blank');
                }


            });              

              

            }


            




          

    });



</script>
@endsection