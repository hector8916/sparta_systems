@extends('layouts.inicio')

@section('content')
<div class="col-md-12">
  <!-- MAP & BOX PANE -->
  <div class="box box-primary">
    <div class="box-header with-border">

      <h3 class="box-title"><img class="img-circle" src="../images/<?php list($confi) = configuracion(); print_r($confi->logo_empresa);?>" alt="User Avatar" heigth="30" width="30"> Ventas Registradas <?php list($confi) = configuracion(); print_r($confi->nombre_empresa);?> </h3>


      <div class="box-tools pull-right">
        
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding">

    	  <table id="example" class="display" style="width:100%">
	        <thead>
	          <tr align="center">
              <th>Fecha de Compra</th>
	            <th>Vendedor</th>
	            <th>Total</th>
	            <th>Cantidad</th>
              <th>Cambio</th>
              <th>Efectivo</th>
	            <th>Acciones</th>
	          </tr>
	        </thead>
	        <tbody>
	          @foreach ($venta_registrada_table as $venta)
                  <tr align="center">
                      <td>{{ $venta->fecha }}</td>
                      <td>{{ $venta->_operador->nombre }} {{ $venta->_operador->apellido_paterno }} {{ $venta->_operador->apellido_materno }}</td>
                      <td>${{ $venta->suma }}</td>
                      <td>{{ $venta->cantidad }}</td>
                      <td>${{ $venta->devolucion }}</td>
                      <td>{{ $venta->monto_pago }}</td>
                      <td >
                        <form action="{{ route('ventas_registradas.destroy',$venta->id )}}" method="POST">
                                      @csrf
                                      @method('DELETE')
                        <div class="btn-group">
                          <button type="button" class="btn btn-warning"><a href="{{ route('ventas_registradas.edit',$venta->id)}}" style="color:#fff;"><i class="fa fa-pencil"></i> Editar</a></button>
                            <button class="btn btn-danger" type="submit"><i class="fa fa-trash"></i> Eliminar</button>
                        </div>
                        </form>
                      </td>
                  </tr>
                @endforeach
	        </tbody>
	      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->

</div>
<script>
  $(document).ready(function() {
      
      $('#example').DataTable({
    
    "scrollY": 300,
    "scrollX": true,
    "pagingType": "full_numbers",
   language: {
    processing:     "Tratamiento en curso...",
    search:         "Buscar&nbsp;:",
    lengthMenu:    "Mostrando _MENU_ registros por página",
    info:           "Mostrando página _START_ a _TOTAL_ Elementos",
    infoEmpty:      "No hay registros disponibles",
    infoFiltered:   "(Filtrado de _MAX_ registros totales)",
    infoPostFix:    "",
    loadingRecords: "Cargando...",
    zeroRecords:    "Ningun Resgistro",
    emptyTable:     "No hay datos disponibles por el momento.",
    paginate: {
        first:      "Primeramente",
        previous:   "Anteriormente",
        next:       "Siguiente",
        last:       "Ultimo"
    },
    aria: {
        sortAscending:  ": Activar para ordenar la columna en orden ascendente.",
        sortDescending: ": Activar para ordenar la columna en orden descendente."
    }
},
dom: 'B<"clear">lfrtip',
    buttons: [
        'colvis'
    ],
});
      
  } );
</script>
@endsection