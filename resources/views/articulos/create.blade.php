@extends('layouts.inicio')

@section('content')
<div class="col-md-12">
	<div class="box box-primary">
	    <div class="box-header with-border">
	    	@isset($articulos)
	    	<h3 class="box-title">Actualizar Articulo</h3>
			@else
			<h3 class="box-title">Nuevo Articulo</h3>
			@endisset
	    </div>
	    <!-- /.box-header -->
	    <!-- form start -->
	    <form role="form">
	      <div class="box-body">
	      	<div class="col-md-6">
	      	<div class="form-group">
	          <label for="exampleInputPassword1">Nombre del Articulo</label>
	          <input type="text" class="form-control" name="nombre" placeholder="Nombre de Articulo" value="@isset($articulos){{ $articulos->nombre }}@endisset">
	        </div>
	    	</div>
	    	<div class="col-md-6">
		        <div class="form-group">
		          <label for="exampleInputEmail1">Descripción</label>
		          <input type="text" class="form-control" name="descripcion" placeholder="Descripción" value="@isset($articulos){{ $articulos->descripcion }}@endisset">
		        </div>
	    	</div>
	    	<div class="col-md-6">
		        <div class="form-group">
		          <label for="exampleInputPassword1">Precio</label>
		          <input type="text" class="form-control" name="precio" placeholder="Precio" value="@isset($articulos){{ $articulos->precio }}@endisset">
		        </div>
		    </div>
		    <div class="col-md-6">
		        <div class="form-group">
		          <label for="exampleInputPassword1">Codigo</label>
		          <input type="text" class="form-control" name="codigo" placeholder="Codigo" value="@isset($articulos){{ $articulos->codigo }}@endisset">
		        </div>
	        </div>
	        <div class="col-md-6">
		        <div class="form-group">
		          <label for="exampleInputPassword1">Stock</label>
		          <input type="text" class="form-control" name="stock" placeholder="Stock" value="@isset($articulos){{ $articulos->stock }}@endisset">
		        </div>
		    </div>
	        <div class="col-md-6">
              	<div class="form-group">
	                <label>Categoria</label>
	                <select class="form-control select2 select2-hidden-accessible" style="width: 100%;"  name="categoria" tabindex="-1" aria-hidden="true">
	                 @isset($articulos)
	                 <option value="{{ $articulos->categoria }}">{{ $articulos->_Categoria->nombre }}</option>
	                 @else
	                 <option>Selecciona una Categoria</option>
	                 @endisset
	                  @foreach($categorias as $categoria)
	                  <option value="{{ $categoria->id }}">{{ $categoria->nombre }}</option>
	                  @endforeach
	                </select>
            	</div>
              
            </div>
	        
	      </div>
	      <!-- /.box-body -->

	      <div class="box-footer">
	      	<a href="{{ url('articulos') }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Regresar</a>
	      	@isset($articulos)
	      	<button type="submit" class="btn btn-primary btn-submit"><i class="fa fa-repeat"></i> Actualizar</button>
	      	@else
	        <button type="submit" class="btn btn-primary btn-submit"><i class="fa fa-save"></i> Agregar</button>
	        @endisset
	      </div>
	    </form>
	  </div>
</div>

<script>
	$(document).ready( function () {


	    $(".btn-submit").click(function(e){

	    
        e.preventDefault();

        var descripcion = $("input[name=descripcion]").val();
        var precio = $("input[name=precio]").val();
        var codigo = $("input[name=codigo]").val();
        var nombre = $("input[name=nombre]").val();
        var stock = $("input[name=stock]").val();
        var categoria = $("select[name=categoria]").val();



        if(descripcion == '' || precio == ''){

        	
        	swal("Upss!", "Lo sentimos Campos Vacios", "warning");
        	

        }else{

        	$.ajax({

	           type:"{{ ( isset($articulos) ? 'PUT' : 'POST' ) }}",

	           url:"{{ ( isset($articulos) ) ? '/articulos/' . $articulos->id : '/articulos/create' }}",
	           headers: {
				      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				     },
	           data:{
	           	descripcion:descripcion,
				precio:precio,
				codigo:codigo,
				nombre:nombre,
				stock:stock,
				categoria:categoria,
				
	           },
	           
	            success:function(data){
	                swal({title: "Felicidades!", text: data.success, type: "success"}, function(){ location.href ="{{ url('articulos') }}"; } );
	            }


	        });

        }

  });
}); 
</script>
@endsection