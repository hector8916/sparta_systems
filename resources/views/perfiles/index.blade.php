@extends('layouts.inicio')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-3">

      <!-- Profile Image -->
      <div class="box box-primary">
        <div class="box-body box-profile">
          <img class="profile-user-img img-responsive img-circle" src="../images/<?php echo Auth::user()->imagen_usuario; ?>" alt="User profile picture">

          <h3 class="profile-username text-center"><?php echo Auth::user()->name; ?></h3>

          <p class="text-muted text-center">
          	<?php echo Auth::user()->_TipoUsuario->nombre; ?>
          </p>

          <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
              <b>Followers</b> <a class="pull-right">1,322</a>
            </li>
            <li class="list-group-item">
              <b>Following</b> <a class="pull-right">543</a>
            </li>
            <li class="list-group-item">
              <b>Friends</b> <a class="pull-right">13,287</a>
            </li>
          </ul>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </div>
    <!-- /.col -->
    <div class="col-md-9">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#settings" data-toggle="tab">Detalles del Usuario</a></li>
        </ul>
        

          <div class="tab-pane" id="settings">
            <form class="form-horizontal">
            @foreach($personal as $persona)
              <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Nombre</label>
                <div class="col-sm-10">
                  <p>{{ $persona->nombre }} {{ $persona->apellido_paterno }} {{ $persona->apellido_materno }}</p>
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail" class="col-sm-2 control-label">Correo Electronico</label>

                <div class="col-sm-10">
                  <p>{{ $persona->correo_electronico }}</p>
                </div>
              </div>
              <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Password</label>

                <div class="col-sm-10">
                  <p>{{ $persona->password }}</p>
                </div>
              </div>
     		@endforeach
            </form>
          </div>
          <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
      </div>
      <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

</section>
@endsection