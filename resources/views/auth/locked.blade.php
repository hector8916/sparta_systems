@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Locked') }}</div>
                    <div class="card-body">
                        <form  aria-label="{{ __('Locked') }}">
                            
                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                                <div class="col-md-6">
                                    <input id="password" type="password" id="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn-submit btn btn-primary">
                                        {{ __('Unlock') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
    $(document).ready( function () {

        $(".btn-submit").click(function(e){

        
        e.preventDefault();

        var password = $('#password').val();

        $.ajax({

               type:"POST",

               url:"/login/create",
               headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     },
               data:{
                password:password
               },
               
                success:function(data){
                    swal({title: "Felicidades!", text: data.success, type: "success"}, function(){ location.href =""; } );
                }


            });
        });
        
    });
    </script>
@endsection