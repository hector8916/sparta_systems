@extends('layouts.inicio')

@section('content')

<div class="col-md-12">
	<div class="box box-primary">
	    <div class="box-header with-border">
	    	@isset($usuarios)
	    	<h3 class="box-title">Actualizar Usuario</h3>
			@else
			<h3 class="box-title">Nuevo Usuario</h3>
			@endisset
	    </div>
	    <!-- /.box-header -->
	    <!-- form start -->

	    @isset($usuarios)
	        <form action="{{ route('usuarios.update',$usuarios->id) }}" method="POST" enctype="multipart/form-data">
	        @csrf
	        @method('PUT')
	      @else
	        <form action="{{ route('usuarios.store') }}" method="POST" enctype="multipart/form-data">
	        @csrf
	      @endisset
	      <div class="box-body">
	      	<div class="col-md-6">
	      	<div class="form-group">
	          <label for="exampleInputPassword1">Nombre del Usuario</label>
	          <input type="text" class="form-control" name="name" placeholder="Nombre de Articulo" value="@isset($usuarios){{ $usuarios->name }}@endisset">
	        </div>
	    	</div>
	    	<div class="col-md-6">
		        <div class="form-group">
		          <label for="exampleInputEmail1">Correo Electronico</label>
		          <input type="text" class="form-control" name="email" placeholder="Correo Electronico" value="@isset($usuarios){{ $usuarios->email }}@endisset">
		        </div>
	    	</div>
	    	<div class="col-md-6">
		        <div class="form-group">
		          <label for="exampleInputPassword1">Password</label>
		          <input type="text" class="form-control" name="password" placeholder="Password" value="@isset($usuarios){{ $usuarios->password_name }}@endisset">
		        </div>
		    </div>

		    

	        <div class="col-md-6">
              	<div class="form-group">
	                <label>Tipo de Usuario</label>
	                <select class="form-control select2 select2-hidden-accessible" style="width: 100%;"  name="tipo_user" tabindex="-1" aria-hidden="true">
	                 @isset($usuarios)
	                	 <option value="{{ $usuarios->tipo_user }}">{{ $usuarios->_TipoUsuario->nombre }}</option>
	                 @else
	                 <option>Selecciona una Categoria</option>
	                 @endisset
	                  
	                </select>
            	</div>
              
            </div>
            
            <div class="col-md-12">

            	<div class="col-md-6 ">
	              	<div class="form-group">
		                <label>Permisos</label>
		                <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" id="id_permiso" name="id_permiso" tabindex="-1" aria-hidden="true">
		                 <option value="0">Selecciona un Permiso</option>
		                  
		                </select>
	            	</div>
	              	<br>
	              	<button type="button" class="btn btn-primary pull-right" id="agregarArticulo"><i class="fa fa-check"></i> Agregar</button>



				      
			        	<button type="button" class="btn btn-primary btn-submit pull-right"><i class="fa fa-check"></i> Agregar Permisos</button>

            	</div>


            	<div class="col-md-6">

	        		 @isset($usuarios)
		            <div class="col-md-6">
		              	<div class="form-group">
			                <label>Imagen de Usuario</label>
			                <div style="height: 20px;"></div>
			                <img src="../../images/{{ $usuarios->imagen_usuario }}" alt="" width="120" height="120">
			                <br>
			                <label>Cambiar Imagen</label>
		                    <input type="file"  name="avatar" value="">
			            </div>
			        </div>


		            
		            @else
		            <div class="col-md-6">
				        <div class="form-group">
				          <label for="exampleInputPassword1">Imagen del Usuario</label>
				          <input type="file"  name="avatar" value="">
				        </div>
				    </div>
				    @endisset
            	</div>

            </div>

		      

		      <!-- /.row -->
		    


           
            
            
            
		    
            @isset($usuarios)
            <input type="hidden" name="id_personal" value="@isset($usuarios){{ $usuarios->id_personal }}@endisset">
            <input type="hidden" name="tipo_user_2" value="@isset($usuarios){{ $usuarios->tipo_user }}@endisset">
            <input type="hidden" name="id" value="@isset($usuarios){{ $usuarios->id }}@endisset">
	        @endisset
	      </div>
	      <!-- /.box-body -->

	      <div class="box-footer">
	      	<a href="{{ url('usuarios') }}" ><button type="button" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Regresar</button></a>
	      	@isset($usuarios)
	      	<button type="submit" class="btn btn-primary "><i class="fa fa-repeat"></i> Actualizar</button>
	      	@else
	        <button type="submit" class="btn btn-primary "><i class="fa fa-save"></i> Agregar</button>
	        @endisset
	      </div>
	    </form>
	  </div>
</div>
@endsection