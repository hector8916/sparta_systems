@extends('layouts.inicio')

@section('content')
    <div class="col-md-12">
      <!-- USERS LIST -->
      <div class="box box-success">
        <div class="box-header with-border">
        @if($users->count() > 0)
          <h3 class="box-title"><img class="img-circle" src="../images/<?php list($confi) = configuracion(); print_r($confi->logo_empresa);?>" alt="User Avatar" heigth="30" width="30"> Personal <?php list($confi) = configuracion(); print_r($confi->nombre_empresa);?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <ul class="users-list clearfix">
            @foreach($users as $user)
            <li>
              <img src="{{ asset('Admin/dist/img/default-user.png') }}" width="80" height="80" alt="User Image" >
              <a class="users-list-name" >{{ $user->name }}</a>
              <a class="users-list-name" >{{ $user->_TipoUsuario->nombre }}</a>
              <span class="users-list-date"><a href="javascript:void(0);" class="chat-toggle" data-id="{{ $user->id }}" data-user="{{ $user->name }}"><button type="button" class="btn btn-primary btn-sm  btn-flat"><i class="fa fa-wechat"></i> Abrir Conversación</button></a></span>
            </li>
            @endforeach
          </ul>
          <!-- /.users-list -->
        </div>
        @else
            <p>No users found! try to add a new user using another browser by going to <a href="{{ url('register') }}">Register page</a></p>
        @endif
      </div>
      <!--/.box -->
    </div>

 
@include('chat-box')
 
    <input type="hidden" id="current_user" value="{{ \Auth::user()->id }}" />
    <input type="hidden" id="pusher_app_key" value="{{ env('PUSHER_APP_KEY') }}" />
    <input type="hidden" id="pusher_cluster" value="{{ env('PUSHER_APP_CLUSTER') }}" />
@stop
 

