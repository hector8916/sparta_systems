@extends('layouts.inicio')

@section('content')
<div class="col-md-12">
	<div class="box box-primary">
	    <div class="box-header with-border">
	    	@isset($tiendas)
	    	<h3 class="box-title">Actualizar Sucursal</h3>
			@else
			<h3 class="box-title">Nueva Sucursal</h3>
			@endisset
	    </div>
	    <!-- /.box-header -->
	    <!-- form start -->
	    <form role="form">
	      <div class="box-body">
	      	<div class="col-md-6">
		      	<div class="form-group">
		          <label for="exampleInputPassword1">Nombre de la Sucursal</label>
		          <input type="text" class="form-control" name="nombre" placeholder="Nombre de la Sucursal" value="@isset($tiendas){{ $tiendas->nombre }}@endisset">
		        </div>
		    </div>

		    <div class="col-md-6">
		      	<div class="form-group">
		          <label for="exampleInputPassword1">Domicilio</label>
		          <input type="text" class="form-control" name="domicilio" placeholder="Domicilio" value="@isset($tiendas){{ $tiendas->domicilio }}@endisset">
		        </div>
		    </div>

		    <div class="col-md-6">
		      	<div class="form-group">
		          <label for="exampleInputPassword1">Horario</label>
		          <input type="text" class="form-control" name="horario" placeholder="Horario" value="@isset($tiendas){{ $tiendas->horario }}@endisset">
		        </div>
		    </div>
	        
	      </div>
	      <!-- /.box-body -->

	      <div class="box-footer">
	      	<a href="{{ url('tiendas') }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Regresar</a>
	      	@isset($tiendas)
	      	<button type="submit" class="btn btn-primary btn-submit"><i class="fa fa-repeat"></i> Actualizar</button>
	      	@else
	        <button type="submit" class="btn btn-primary btn-submit"><i class="fa fa-save"></i> Agregar</button>
	        @endisset
	      </div>
	    </form>
	  </div>
</div>

<script>
	$(document).ready( function () {


	    $(".btn-submit").click(function(e){

	    
        e.preventDefault();

        var nombre = $("input[name=nombre]").val();
        var domicilio = $("input[name=domicilio]").val();
        var horario = $("input[name=horario]").val();




        if(nombre == '' ||  domicilio == '' ||  horario == ''){

        	
        	swal("Upss!", "Lo sentimos Campos Vacios", "warning");
        	

        }else{

        	$.ajax({

	           type:"{{ ( isset($tiendas) ? 'PUT' : 'POST' ) }}",

	           url:"{{ ( isset($tiendas) ) ? '/tiendas/' . $tiendas->id : '/tiendas/create' }}",
	           headers: {
				      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				     },
	           data:{
				nombre:nombre,
				domicilio:domicilio,
				horario:horario,
	           },
	           
	            success:function(data){
	                swal({title: "Felicidades!", text: data.success, type: "success"}, function(){ location.href ="{{ url('tiendas') }}"; } );
	            }


	        });

        }

  });
}); 
</script>
@endsection