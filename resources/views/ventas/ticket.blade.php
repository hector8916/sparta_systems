<!DOCTYPE html>
<html>
    <head>
        <script src="{{ asset('Admin/bower_components/jquery/dist/jquery.min.js') }}"></script>
        <style type="text/css" media="screen">
            * {
                font-size: 12px;
                font-family: 'Times New Roman';
            }

            td,
            th,
            tr,
            table {
                border-top: 1px solid black;
                border-collapse: collapse;
            }

            td.producto,
            th.producto {
                width: 75px;
                max-width: 75px;
            }

            td.cantidad,
            th.cantidad {
                width: 40px;
                max-width: 40px;
                word-break: break-all;
            }

            td.precio,
            th.precio {
                width: 40px;
                max-width: 40px;
                word-break: break-all;
            }

            .centrado {
                text-align: center;
                align-content: center;
            }

            .ticket {
                width: 155px;
                max-width: 155px;
            }

            img {
                max-width: inherit;
                width: inherit;
            }
        </style>
    </head>
    <body>
        <div class="ticket">
            <img
                src="../images/<?php list($confi) = configuracion(); print_r($confi->logo_empresa);?>"
                alt="Logotipo">
            <p class="centrado"><?php list($confi) = configuracion(); print_r($confi->nombre_empresa);?>
                <br><?php list($confi) = configuracion(); print_r($confi->direccion);?>
                <br>Lo Atendio : {{ $usuario_atendio }}
                <br>{{ $id }}</p>
            <table>
                <thead>
                    <tr>
                        <th class="cantidad">CANT</th>
                        <th class="producto">PRODUCTO</th>
                        <th class="precio">$$</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="cantidad">1.00</td>
                        <td class="producto">CHEETOS VERDES 80 G</td>
                        <td class="precio">$8.50</td>
                    </tr>
                    <tr>
                        <td class="cantidad"></td>
                        <td class="producto">TOTAL</td>
                        <td class="precio">${{$venta}}</td>
                    </tr>
                </tbody>
            </table>
            <p class="centrado">¡GRACIAS POR SU COMPRA!
                <br>Dudas o Aclaraciones llamar al <?php list($confi) = configuracion(); print_r($confi->telefono);?> o
                <br>mandar email a <?php list($confi) = configuracion(); print_r($confi->correo_electronico);?></p>
        </div>
       <script>
           window.print();
       </script>
    </body>
</html>