@extends('layouts.inicio')

@section('content')

    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <img class="img-circle" src="../images/<?php list($confi) = configuracion(); print_r($confi->logo_empresa);?>" alt="User Avatar" heigth="30" width="30"> Ventas <?php list($confi) = configuracion(); print_r($confi->nombre_empresa);?>
            <small class="pull-right">Fecha: <?php echo date('Y-m-d') ?>, Hora:<?php date_default_timezone_set('America/Mexico_City');echo date('H:i') ?></small>
            <input type="hidden" value="<?php echo date('Y-m-d h:i:s') ?>" name="hora">
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-12 invoice-col">
          <div class="col-md-4 ">
              	<div class="form-group">
	                <label>Articulo</label>
	                <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" id="id_articulo" name="id_articulo" tabindex="-1" aria-hidden="true">
	                 <option>Selecciona un Articulo</option>
	                  @foreach($articulos as $articulo)
	                  <option value="{{ $articulo->id }}">{{ $articulo->nombre }}</option>
	                  @endforeach
	                </select>
            	</div>
              
            </div>

            <div class="col-md-4">
                <div class="form-group">
                  <label>Stock de Articulo</label>
                  <p id="stock"></p>
              </div>
              
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label>Cantidad</label>
                <input type="text" class="form-control" id="id_cantidad" placeholder="Ingresa la Cantidad">
            </div>

              
            <div class="col-md-4">
            </div>
            <button type="button" class="btn btn-primary pull-right" id="agregarArticulo"><i class="fa fa-check"></i> Agregar</button>
        </div>


        <!-- /.col -->
      </div>
      <!-- /.row -->
      <hr>
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped " id="tablaArticulo">
            <thead>
            <tr align="center">
              <th>Cantidad</th>
              <th>Producto</th>
              <th>Serial #</th>
              <th>Description</th>
              <th>Subtotal</th>
              <th>Accion</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <hr>
      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead" style="visibility: hidden;">Métodos de pago:</p>
          <div class="table-responsive">
            <table class="table">
              <tr>
                <th>Tipo de Cambio:</th>
                <td>
                  <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <input type="text" class="form-control" name="monto_pago" id="monto_pago">
                    <span class="input-group-addon">.00</span>
                  </div>
                </td>
              </tr>
              <tr>
                <th>Cambio: $</th>
                <td><p id="devolucion"></p></td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead">Monto de la Venta</p>

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th>Cantidad:</th>
                <td><p id="total_cantidad"></p></td>
              </tr>
              <tr>
                <th>Total: $</th>
                <td ><p id="total_productos"></p></td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          
          <div class="btn-submit btn btn-primary pull-right"><i class="fa fa-shopping-cart"></i> Pagar</div>


          <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#exampleModal"><i class="fa fa-user"></i> Nuevo Cliente</button>

        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h2 class="modal-title" id="exampleModalLabel"><i class="fa fa-user"></i> Agregar Nuevo Cliente</h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputPassword1">Nombre del Cliente</label>
                  <input type="text" class="form-control" name="nombre" placeholder="Nombre del Cliente" >
                </div>

                <div class="form-group">
                  <label for="exampleInputPassword1">Apellido Paterno</label>
                  <input type="text" class="form-control" name="apellido_paterno" placeholder="Apellido Paterno" >
                </div>

                <div class="form-group">
                  <label for="exampleInputPassword1">Apellido Materno</label>
                  <input type="text" class="form-control" name="apellido_materno" placeholder="Apellido Materno">
                </div>

                <div class="form-group">
                  <label for="exampleInputPassword1">Correo Electronico</label>
                  <input type="text" class="form-control" name="correo_electronico" placeholder="Correo Electronico" >
                </div>
                
              </div>
              <!-- /.box-body -->
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            <button type="button" class="btn-submit-cliente btn btn-primary">Guardar Cliente</button>
          </div>
        </div>
      </div>
    </div>


    <div class="modal "  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
              <div id="myPrintArea" >
                <div id="ticket_vendido"></div>
                <div class="ticket" style="width: 100%;" id="bcTarget"></div>
              </div>
        </div>
      </div>
    </div>



<script>
  var arrayAsp = [];
    
  var objAsp = {};

  var arrayEspaciolact = [];

///////////////////////////////////////////////////////////////////////////////////////
$( "#id_articulo" ).change(function() {
  var articulo_id = $('#id_articulo').val();
              $.ajax({
               type:"POST",
               url:"/ventas/articulo",
               headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
               data:{
                articulo_id:articulo_id
               },
               
                success:function(data){
                  var stock = data.stock.stock;

                  $('#stock').html('<input type="text" class="form-control" value="'+stock+'" disabled>');
                }
              });
});
    /////////////////// nuevo cliente //////////////////////////////

      $(".btn-submit-cliente").click(function(e){

      
        e.preventDefault();

        var nombre = $("input[name=nombre]").val();
        var apellido_paterno = $("input[name=apellido_paterno]").val();
        var apellido_materno = $("input[name=apellido_materno]").val();
        var correo_electronico = $("input[name=correo_electronico]").val();




        if(nombre == '' || apellido_paterno == '' || correo_electronico == '' ){

          
          swal("Upss!", "Lo sentimos Campos Vacios", "warning");
          

        }else{

          $.ajax({

             type:"{{ ( isset($clientes) ? 'PUT' : 'POST' ) }}",

             url:"{{ ( isset($clientes) ) ? '/clientes/' . $clientes->id : '/clientes/create' }}",
             headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
             data:{
        nombre:nombre,
        apellido_paterno:apellido_paterno,
        apellido_materno:apellido_materno,
        correo_electronico:correo_electronico,
             },
             
              success:function(data){
                  swal({title: "Felicidades!", text: data.success, type: "success"}, function(){  } );
                  $('#exampleModal').modal('toggle');
              }


          });

        }

  });
//////////////////////////////////////////////////

 $(document).on("click","#agregarArticulo",function(e){
      e.preventDefault();

     
      var cantidad = $('#id_cantidad').val();
      var articulo_id = $('#id_articulo').val();
      var articulo = $('select[name="id_articulo"] option:selected').text();




      

      if (cantidad == '' || articulo_id == '' ) {
                swal({title: "Cuidado!", text: "campos vacios", type: "warning"}, function(){ } );
      }else{
        
      $.ajax({
        url: 'ventas/'+articulo_id+'/traerArticulo',
        type: "GET",
        headers:  {'X-CSRF-TOKEN':$("meta[name='csrf-token']").attr('content')},
        dataType:'json',
          success:function(data)
          {

            otrosSQL = data.articulos;
            for($a = 0; $a < otrosSQL.length; $a++) {

              var precio = otrosSQL[$a].precio;
              var codigo = otrosSQL[$a].codigo;
              var descripcion = otrosSQL[$a].descripcion;
              




             var precio_subtotal =  parseFloat(otrosSQL[$a].precio) * parseFloat(cantidad);

                    
                    objAsp = {
                      cantidad: $('#id_cantidad').val(),
                      articulo_id : $('#id_articulo').val(),
                      articulo: $('select[name="id_articulo"] option:selected').text(),
                      precio : precio_subtotal,
                      codigo : codigo,
                      descripcion : descripcion,


                    }


                    agregarAsp(objAsp);
                    arrayAsp.push(objAsp);

                    
                    ///////////// suma de total ////////////
                    var numero =[];
                    var suma = 0;

                    var numero_ca =[];
                    var suma_ca = 0;


                     for($o = 0; $o < arrayAsp.length; $o++) {

                          numero[$o] = arrayAsp[$o].precio;
                          suma = suma + numero[$o];


                          numero_ca[$o] = arrayAsp[$o].cantidad;
                          suma_ca =   parseFloat(numero_ca[$o]) + parseFloat(suma_ca);
                         

                          
                       }

                       $('#total_productos').html('<div class="input-group">'+
                                                  '<span class="input-group-addon">$</span>'+
                                                  '<input type="text" class="form-control" name="suma" value="'+suma+'" disabled>'+
                                                  '<span class="input-group-addon">.00</span>'+
                                                  '</div>');

                      

                       $('#total_cantidad').html('<div class="input-group">'+
                                                  '<span class="input-group-addon"><i class="fa fa-cart-arrow-down"></i></span>'+
                                                  '<input type="text" class="form-control" name="cantidad" value="'+suma_ca+'" disabled>'+
                                                  '</div>');


                       $("#monto_pago").change(function(){
                        

                          var devolucion_monto =  parseFloat($('#monto_pago').val() - parseFloat(suma) );

                          $('#devolucion').html('<div class="input-group">'+
                                                  '<span class="input-group-addon">$</span>'+
                                                  '<input type="text" class="form-control" name="devolucion" value="'+devolucion_monto+'" disabled>'+
                                                  '<span class="input-group-addon">.00</span>'+
                                                  '</div>');
                        });




              

              
              
            }
            
          }
      });

    }


      
  });
 var contador_asp = 0;
	function agregarAsp(objAsp){
	  //construccion de tabla
	    var tr = '<tr class="borrar_aspc_create" id="filas'+contador_asp+'">'+
	    
	    '<td>'+objAsp.cantidad+'<input type="hidden" name="id_materia[]" value="'+objAsp.cantidad+'" id="nvel_educativo_nombre'+contador_asp+'"/></td>'+
	    '<td>'+objAsp.articulo+'<input type="hidden" name="id_grupo[]" value="'+objAsp.articulo+'" id="nvel_educativo_nombre'+contador_asp+'"/></td>'+
      '<td>'+objAsp.codigo+'<input type="hidden" name="id_grupo[]" value="'+objAsp.codigo+'" id="nvel_educativo_nombre'+contador_asp+'"/></td>'+
      '<td>'+objAsp.descripcion+'<input type="hidden" name="id_grupo[]" value="'+objAsp.descripcion+'" id="nvel_educativo_nombre'+contador_asp+'"/></td>'+
      '<td>$'+objAsp.precio+'<input type="hidden" name="id_grupo[]" value="'+objAsp.precio+'" id="precios_total'+contador_asp+'"/></td>'+

	    '<td style=" text-align: center; "><div  class="btn btn-danger" onclick="eliminarAsp_create('+contador_asp+')"> <i class="fa fa-trash"></i></div></td>'
	    '</tr>';




      
	    $("#tablaArticulo").append(tr);
	    //limpiar campos
	    $('#id_cantidad').val('');
	    $('#id_articulo').val('');
	    
	 contador_asp ++;
   //////////////////////////////////////
                    
	}

//$(document).on("click",".borrar_figura",function(e){
//      var id_figura_nueva = $(this).attr('figura_nueva_id');
//      e.preventDefault();
//      var id = $('#figura_nueva').val();
//      arrayFiguras.splice(id, 1);
//      $('#id-figura-'+id_figura_nueva).remove();
//  });

	function eliminarAsp_create(id) {
	    
	    
	    arrayAsp.splice(id,1);

	      $('#filas'+id).remove();
	    }


///////////////////////////////////////////



        $(".btn-submit").click(function(e){
          e.preventDefault();
            

            var suma = $("input[name=suma]").val();
            var cantidad = $("input[name=cantidad]").val();
            var devolucion = $("input[name=devolucion]").val();
            var monto_pago = $("input[name=monto_pago]").val();
            var hora = $("input[name=hora]").val();



            if(monto_pago == 0){
              swal({title: "Cuidado!", text: "El Ingresar el monto de pago", type: "warning"}, function(){ } );
            }else{
              $.ajax({

               type:"POST",
               url:"/ventas/create",
               headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
               data:{
                hora:hora,
                suma:suma,
                cantidad:cantidad,
                devolucion:devolucion,
                monto_pago:monto_pago,
                productos:arrayAsp
               },
               
                success:function(data){

                    swal({title: "Felicidades!", text: data.success, type: "success"}, function(){ location.href ="{{ url('ventas') }}"; } );


                  $.ajax({

                   type:"POST",
                   url:"/ventas/ticket_compra",
                   headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                   },
                   data:{
                    id_venta:data.id,
                  
                   },
                   
                    success:function(data){
                      //console.log(data.venta);

                      var array_venta = data.venta.venta;
                      var array_producto = data.venta.productos;


                      for($o = 0; $o < array_venta.length; $o++) {

                          var total = array_venta[$o].suma;
                          var monto_pago = array_venta[$o].monto_pago;
                          var devolucion = array_venta[$o].devolucion;
                          var fecha = array_venta[$o].fecha;
                          var serial = array_venta[$o].serial;


                           

                          $("#bcTarget").barcode(""+serial+"", "code11"); 


                          var totales = '<tr>'+
                                '<td class="centrado" style="font-size:12px;"></td>'+
                                '<td class="centrado" style="font-size:12px;"><strong>TOTAL</strong></td>'+
                                '<td class="centrado" style="font-size:10px;">$'+total+'</td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td class="centrado" style="font-size:12px;"></td>'+
                                '<td class="centrado" style="font-size:12px;"><strong>PAGO</strong></td>'+
                                '<td class="centrado" style="font-size:10px;">$'+monto_pago+'</td>'+
                            '</tr>'+
                            '<tr>'+
                                '<td class="centrado" style="font-size:12px;"></td>'+
                                '<td class="centrado" style="font-size:12px;"><strong>CAMBIO</strong></td>'+
                                '<td class="centrado" style="font-size:10px;">$'+devolucion+'</td>'+
                            '</tr>';


                            

                            
                          
                       }

                      var productillos = [];
                      for ($i = 0; $i < array_producto.length; $i++) {

                       productillos +='<tr>'+
                                        '<td class="centrado" style="font-size:10px;">'+array_producto[$i].cantidad+'</td>'+
                                        '<td class="centrado" style="font-size:10px;">'+array_producto[$i].articulo+'</td>'+
                                        '<td class="centrado" style="font-size:10px;">$'+array_producto[$i].precio+'</td>'+
                                        '</tr>';

                      }$i++;



                      var ticket =
                                '<!DOCTYPE html>'+
                                '<head>'+
                                '<link rel="stylesheet" href="{{ asset("Admin/dist/css/imprimir.css") }}">'+
                                '</head>'+
                                '<body class="cuerpo">'+
                                    '<div class="ticket">'+
                                    '<img  src="../images/<?php list($confi) = configuracion(); print_r($confi->logo_empresa);?>"alt="Logotipo" heigth="150" width="150">'+
                                        '<p class="centrado" style="font-size:12px;"><?php list($confi) = configuracion(); print_r($confi->nombre_empresa);?>'+
                                            '<br><?php list($confi) = configuracion(); print_r($confi->direccion);?>'+
                                            '<br>Sucursal:'+data.venta.sucursal+
                                            '<br>Domicilio:'+data.venta.sucursal_domicilio+
                                            '<br>Lo Atendio :'+data.venta.usuario_atendio+
                                            '<br>'+fecha+'</p>'+
                                        '<table style="width:100%;">'+
                                            '<thead>'+
                                                '<tr>'+
                                                    '<th class="centrado" style="font-size:12px;">CANT</th>'+
                                                    '<th class="centrado" style="font-size:12px;">PRODUCTO</th>'+
                                                    '<th class="centrado" style="font-size:12px;">$</th>'+
                                                '</tr>'+
                                            '</thead>'+
                                            '<tbody class="productillo totales">'+
                                            '</tbody>'+
                                        '</table>'+
                                        '<p class="centrado" style="font-size:10px;">¡GRACIAS POR SU COMPRA!'+
                                            '<br>Dudas o Aclaraciones llamar al <?php list($confi) = configuracion(); print_r($confi->telefono);?> o'+
                                            '<br>mandar email a <?php list($confi) = configuracion(); print_r($confi->correo_electronico);?></p>'+
                                          '</div>'+
                                      '</body>'+
                                  '</html>';

                            $("#ticket_vendido").append(ticket);
                            
                            $(".productillo").append(productillos);
                            $(".totales").append(totales);
                      $("div#myPrintArea").printArea();
                    }
                });              
                    


                }


            });
              ////////////// ticket compra /////////////////////////////
              

              

            }


            




          

    });



</script>
@endsection