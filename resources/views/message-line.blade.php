@if($message->from_user == \Auth::user()->id)

    <div class="direct-chat-msg right base_sent" data-message-id="{{ $message->id }}">

      <!-- /.direct-chat-info -->
      <img class="direct-chat-img" src="{{ asset('chat-assets/images/user-avatar.png') }}" alt="message user image">
      <!-- /.direct-chat-img -->
      <div class="direct-chat-text msg_sent">
        <p>{!! $message->content !!}</p>
                    <time datetime="{{ date("Y-m-dTH:i", strtotime($message->created_at->toDateTimeString())) }}">{{ Auth::user()->name }} • {{ $message->created_at->diffForHumans() }}</time>
      </div>
      <!-- /.direct-chat-text -->
    </div>
 
@else

<div class="direct-chat-msg base_receive" data-message-id="{{ $message->id }}">

  <!-- /.direct-chat-info -->
  <img class="direct-chat-img" src="{{ asset('chat-assets/images/user-avatar.png') }}" alt="message user image">
  <!-- /.direct-chat-img -->
  <div class="direct-chat-text msg_receive">
    <p>{!! $message->content !!}</p>
                <time datetime="{{ date("Y-m-dTH:i", strtotime($message->created_at->toDateTimeString())) }}">{{ Auth::user()->name }} • {{ $message->created_at->diffForHumans() }}</time>
  </div>
  <!-- /.direct-chat-text -->
</div>

 
@endif


