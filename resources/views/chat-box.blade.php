

<div class="box box-success direct-chat direct-chat-success " id="chat_box" style="display: none">
                <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-comments-o"></i> Chat Directo con <i class="chat-user"></i></h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool close-chat" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <!-- Conversations are loaded here -->
                  <div class="direct-chat-messages chat-area">
                    
                    

                  </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">

                    <div class="input-group">
                      <input type="text" name="message" placeholder="Escribir Mensaje..." class="form-control input-sm chat_input">
                      <span class="input-group-btn">
                            <button type="button" class="btn btn-primary btn-sm btn-chat btn-flat"><i class="fa fa-send"></i> Enviar</button>
                          </span>
                    </div>

                </div>
                <!-- /.box-footer-->
                <input type="hidden" id="to_user_id" value="" />
              </div>

